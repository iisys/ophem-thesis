\chapter{Pattern extraction}
\label{chp:patternextraction}

This chapter describes how values for each feature are extracted from the preprocessed dataset. These values are input for a machine learning algorithm in the next phase, which will be discussed in \cref{chp:modeltraining}. Pattern extraction entails only basic ETL operations for some features, as \cref{sec:patternextraction:usage,sec:patternextraction:lhf} describe for the system usage and low-hanging fruit features respectively, but uses more complicated algorithms for others, as \cref{sec:patternextraction:rep} describes for the repetitive tasks features.


%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------
\section{System usage}
\label{sec:patternextraction:usage}
Seeing as there is no data available about the time users spend on each page, this is measured indirectly by looking at the time the user spends browsing from page to page. This means a simple summation of intervals between a user’s requests is sufficient. Recall from \cref{sec:dataprep:usage} that each log entry in the preprocessed dataset includes the current and the next timestamp, as well as the current and the next type of log entry. A too large interval between requests indicates user inactivity. The example of a user reading or writing a lengthy report for several minutes is a reason to make the interval not too small. For that reason, a maximum of \featureusageIdleTime\ was chosen between subsequent requests. If two requests are more than \featureusageIdleTime\ apart, it is assumed that the user landed on its target page. This means it is essentially a partitioning into sessions with an expiration time of \featureusageIdleTime. These are the conditions considered per feature, for each log entry:
\begin{description}
    \item[\creffulldf{df:prev:total}] If the next timestamp minus the current timestamp is smaller than \featureusageIdleTime, add that interval to the user's total system usage.
    \item[\creffulldf{df:prev:total:napi}] If the next timestamp minus the current timestamp is smaller than \featureusageIdleTime\ and the current entry's log type is \napi, add that interval to the user's total \somtoday\ usage.
    \item[\creffulldf{df:prev:total:api}] If the next timestamp minus the current timestamp is smaller than \featureusageIdleTime\ and the current entry's log type is \api, add that interval to the user's total \somdocent\ usage.
    \item[\creffulldf{df:prev:total:switch}] If the next timestamp minus the current timestamp is smaller than \featureusageIdleTime\ and the current entry's log type is different than the next entry's log type, increment the user's total interface switch count by one.
\end{description}
Note that, as mentioned in \cref{subsubsec:featurediscovery:results:features:prevalence}, a disadvantage of this indirect measurement method is that the last visited page of a user's session is not taken into account for the user's system usage time, i.e. \cref{df:prev:total,df:prev:total:napi,df:prev:total:api}.



%----------------------------------------------------------------------------------------
%	SECTION 2
%----------------------------------------------------------------------------------------
\section{Repetitive tasks}
\label{sec:patternextraction:rep}

In a collection of sequences, sequential pattern\footnote{ In this section a pattern is a sequential pattern: an ordered series of items. In the rest of this thesis a pattern is a user's usage pattern.} mining discovers patterns of elements that are often found in the same subsequent order. Scientific literature mentions three requirements that a pattern must meet to be eligible for further analysis: the minimum pattern length (\textit{minpatternlength}) that specifies the minimum amount of elements in the pattern; the minimum support (\textit{minsupport}) that says in at least how many of the sequences the pattern must occur and which relates directly to our \textit{minimum inter-session occurrence} requirement mentioned in \cref{subsubsec:featurediscovery:results:features:rep}; and the minimum repetition support (\textit{minrepsupport}) that dictates the minimum amount of occurrences of the pattern in each sequence and which relates directly to our \textit{minimum intra-session occurrence} requirement. The multitude of variations on each sequential pattern mining algorithm type adds and/or removes several pattern requirements. These ones mentioned here are the most basic and interesting, especially because they map directly onto our requirements for \cref{df:rep:pat,df:rep:iter,df:rep:weighted}. To clarify: the patterns that are found using these algorithms are the repetitive tasks that we are looking for.

In our initial literature study, no algorithm was identified that satisfied our requirements for \cref{df:rep:pat,df:rep:iter,df:rep:weighted}. A new algorithm was constructed to find the repetitive tasks we are looking for. The steps of our algorithm, Apriorirep1i, are based on the frequent items analysis algorithm Apriori and its sequential pattern mining extension AprioriAll~\cite{Agrawal1995}. This was chosen to support intuitive comprehension of the algorithm, especially in the maximal phase (explained later on). In hindsight, Apriorirep1i turned out to practically take a PrefixSpan approach, but breadth-first (finding all patterns of the same length) instead of depth-first (finding patterns of increasing length with the same prefix). Nearing the end of this project we found that important work was overlooked in our initial search that would have altered our approach to Apriorirep1i. Related work, including the overlooked work, is discussed in \cref{subsec:apriorirep1i:relatedwork}.
It should be noted that the implementation of a different algorithm (along with the validation of its correct execution) would have required a slightly less but similar amount of time than was spent on Apriorirep1i.

Semantic differences between the algorithms found in literature and the one required for repetitive system actions are shown in \cref{tab:apriorirep1i:semdif}. Especially notice the semantics of the itemset concept, because this is the most influential and practical difference. The difference in sequence database semantics means that the algorithm in our case is applied on each user instead of the complete dataset. Also note that an action is the same as a request. 

The basic algorithm is discussed in \cref{subsec:apriorirep1i}, an extension that makes it complete in \cref{subsec:apriorirep1iextension} and its application on the preprocessed dataset in \cref{subsec:apriorirep1iapplication}. The similarities and differences with regard to previous work are discussed in \cref{subsec:apriorirep1i:relatedwork}. 

\begin{table}[h]\centering
	\begin{threeparttable}
		\centering\bgroup\def\arraystretch{1.3}
		\caption[Semantic use case differences from literature]{Semantic differences between use cases of the algorithms found in literature and our algorithm}
		\label{tab:apriorirep1i:semdif}
		\begin{tabular}{l | p{6.5cm} p{2.3cm}}
			\hline
			\multicolumn{1}{l}{concept} & web shop records                               & \nl{web system\\usage logs} \\ \hline\hline
			sequence database           & complete history of bought products            & one user                    \\
			sequence                    & ordered history of bought products of one user & user session                \\
			itemset / element           & set of products bought together                & action\tnote{1}             \\
			item                        & bought product                                 & action                      \\ \hline
		\end{tabular}\egroup
		\begin{tablenotes}
			\item[\tiny{1}] \footnotesize{Due to the nature of web system usage logs, a user can not perform two actions at the same time. Consequently, itemsets are always of length 1.}
		\end{tablenotes}
	\end{threeparttable}
\end{table}



\subsection{Apriorirep1i (basic)}
\label{subsec:apriorirep1i}

%The frequent items analysis algorithm Apriori and its sequential pattern mining extension AprioriAll~\cite{Agrawal1995} are used as the basis for Apriorirep1i. % Don't say this again.
This algorithm was made while keeping in mind that results had priority over optimal performance. A few measurements are given in \cref{subsec:apriorirep1iapplication} to indicate that this was not an issue. The Python 2.7 implementation that was used for this study can be found in \cref{app:apriorirep1i} and online\footnote{ \url{https://karim.elass.al/masterthesis/apriorirep1i}}. \\

\did{Terminology}
% Terminology
The support is in literature defined as a relative value. In the case of the pattern support, for instance, it is the fraction of total sequences that support that pattern. However, in \cref{subsubsec:featurediscovery:results:features:rep} we defined the inter- and intra-session occurrences as absolute values. Since there are no practical advantages or disadvantages to using one or the other in our situation and since existing algorithms internally use the absolute value anyway, from now on the absolute value is meant when talking about the support.

Literature defines large itemsets or \textit{litemsets} as itemsets that have the minimum support. Seeing as Apriorirep1i works with itemsets of length one, \textit{litems} or large items are the equivalent of large itemsets of length one.

The algorithm uses the data structure \textit{occurrence set} internally and as its output. This is a map of patterns mapped to their occurrences. The occurrences are maps, where the zero-based sequence indices of the sequence database are mapped to a set of zero-based indices pointing to the starting occurrence of the pattern within the sequence. To illustrate, the following is an occurrence set of two patterns occurring in the example sequence database of \cref{tab:apriorirep1i:runningexample}: $ ("abcd"\rightarrow(4\rightarrow\{0,4,8\},5\rightarrow\{0,5,9\}), "de"\rightarrow(5\rightarrow\{3, 12\})) $. An advantage of this structure is that information about a pattern's occurrences is retained, while metrics can be obtained in a computationally cheap way by counting the elements in the lists of the occurrence set.

% Running example
As a running example, we take the sequence database shown in \cref{tab:apriorirep1i:runningexample}. The example includes a fair amount of self-succeeding cyclical occurrences. This was chosen to show that even though this algorithm finds both cyclical and non-cyclical repetitive patterns, it is not perfect. Note that this algorithm is independent of the lexical value of the element symbols: replacing element \spattern{a} with \spattern{z} yields the same results.
\begin{table}[h!]\centering\bgroup\def\arraystretch{1.3}
	\caption[Running example for Apriorirep1i illustration]{The sequence database used as a running example to illustrate the workings of Apriorirep1i and has parameters minsupport=2, minrepsupport=2 and minpatternlength=2}
	\label{tab:apriorirep1i:runningexample}
	\begin{tabular}{cl}
		\hline
		\nlc{{\small seq.} \\ {\small index}} & sequence                    \\ \hline\hline
		                  0                   & a b c a c                   \\
		                  1                   & a c t y                     \\
		                  2                   & a b c a b c a b             \\
		                  3                   & b c a b c a b c a b c       \\
		                  4                   & a b c d a b c d a b c d     \\
		                  5                   & a b c d e a b c d a b c d e \\
		                  6                   & x y z                       \\ \hline
	\end{tabular}\egroup
\end{table}

The patterns in the example that can be identified are \spattern{abcd}, \spattern{abc}, \spattern{bca} and \spattern{cab}. As one might notice, \spattern{abc}, \spattern{bca} and \spattern{cab} are the cyclically shifted versions of each other resulting from their self-succeeding occurrences. Should all of these patterns be identified as frequent patterns, or only one of them? This self-succeeding pattern origin ambiguity is addressed in \cref{subsec:apriorirep1iextension}. \\

% Basis
%The apriori algorithm was taken as a basis to ensure comprehensability, specifically the AprioriAll variant~\cite{Agrawal1995}. % No, don't say it AGAIN.

The input expected by Apriorirep1i is a sequence database and the parameters minsupport, minrepsupport and minpatternlen, having constraints minsupport>0, minrepsupport>1 and minpatternlen>1.
Before the different phases are discussed, one step needs to be explained that is applied several times throughout the algorithm: the \texttt{purge\_nonlpatterns} step. The term \textit{lpatterns} (or \textit{large patterns}) follows the equivalent definition of \textit{litems}, but applied to repetitive patterns. The occurrence set of an lpattern adheres to the minrepsupport and minsupport requirements. This step works in this order:
\begin{enumerate}
	\item Only occurrences are retained of sequences that adhere to the \texttt{minrepsupport} requirement, so this is evaluated per sequence.
	\item Only patterns are retained that adhere to the \texttt{minsupport} requirement, so this is evaluated per pattern.
\end{enumerate}

The algorithm overview is shown in \cref{lst:apriorirep1i}. The differences with AprioriAll are discussed next, per phase. Note that the \texttt{purge\_cyclic\_shifts} and \texttt{purge\_interconnecting\_subpatterns} steps are discussed in \cref{subsec:apriorirep1iextension}.
%\begin{lstlisting}[language=Python,caption={Apriorirep1i algorithm},label={lst:apriorirep1i},numbers=left,mathescape,escapechar=|]
%\begin{listing}
%\end{listing}
\begin{mylisting}\captionof{listing}{Apriorirep1i algorithm}\label{lst:apriorirep1i}
\begin{minted}[escapeinside=||,breaklines=true,linenos=true,fontsize=\footnotesize,tabsize=4]{python}
# IN: sequence database -> sequence_db
#                   int -> minsupport
#                   int -> minrepsupport
#                   int -> minpatternlen
# OUT: occurrence set

# litem phase
|$L_1$| = get_litems(sequence_db, minsupport, minrepsupport)

# transformation phase
|\Dt| = truncate_nonlitem_sequences(sequence_db, |$L_1$|)

for (k = 2; |$L_{k-1} \neq \emptyset$|; k++):
	# sequence phase
	|$L_k$| = get_next_patterns(|\Dt|, |$L_{k-1}$|)
	|$L_k$| = purge_nonlpatterns(|$L_k$|, minsupport, minrepsupport)
	
	|$L_k$| = purge_subpattern_cycle_start(|$L_k$|, |$L_{k-1}$|, |\Dt|)
	|$L_k$| = purge_cyclic_shifts(|$L_k$|, |\Dt|)
	|$L_k$| = purge_nonlpatterns(|$L_k$|, minsupport, minrepsupport)
	
	# maximal phase
	if k - 1 < minpatternlen:
		delete |$L_{k-1}$|
	else:
		|$L_{k-1}$| = purge_interconnecting_subpatterns(|$L_{k-1}$|, |$L_k$|)
	
		|$L_{k-1}$| = purge_subpatterns(|$L_{k-1}$|, |$L_k$|)

|$L$| = purge_nonlpatterns(|$L$|, minsupport, minrepsupport)
return |$L$|
\end{minted}
\end{mylisting}

% Phases
 % Sort phase
 AprioriAll starts with a \textbf{sort phase} in which the original database with customer transactions is transformed into a sequence database. To increase generality, Apriorirep1i expects a sequence database and thus skips the sort phase.
 
 % Litemset phase
 AprioriAll's \textbf{litemset phase} focuses on finding all litemsets. Seeing as we work with items instead of itemsets, Apriorirep1i applies a litem phase. The sequence database is traversed, storing each item encounter. The \texttt{purge\_nonlpatterns} step is applied and the result is the litem occurrence set $L_1$. In the case of our running example, $L_1$ is shown in \cref{tab:apriorirep1i:re:litemphase}. An future optimization to reduce memory space is to apply occurrence purging based on the minrepsupport requirement during the litem phase.
 \begin{table}[h!]\centering\bgroup\def\arraystretch{1.3}
 	\caption[Result of Apriorirep1i's litem phase (running example)]{The result $L_1$ after Apriorirep1i's litem phase of our running example}
 	\label{tab:apriorirep1i:re:litemphase}
 	\begin{tabular}{cllll}
 		\hline
 		\tsi\ & \tblocc{a}    & \tblocc{b}       & \tblocc{c}        & \tblocc{d}     \\ \hline\hline
 		$s_0$ & $\{0, 3\}$    &                  & $\{2, 4\}$        &                \\
 		$s_2$ & $\{0, 3, 6\}$ & $\{1, 4, 7\}$    & $\{2, 5\}$        &                \\
 		$s_3$ & $\{2, 5, 8\}$ & $\{0, 3, 6, 9\}$ & $\{1, 4, 7, 10\}$ &                \\
 		$s_4$ & $\{0, 4, 8\}$ & $\{1, 5, 9\}$    & $\{2, 6, 10\}$    & $\{3, 7, 11\}$ \\
 		$s_5$ & $\{0, 5, 9\}$ & $\{1, 6, 10\}$   & $\{2, 7, 11\}$    & $\{3, 8, 12\}$ \\ \hline
 	\end{tabular}\egroup
 \end{table}

 % Transformation phase
 The next phase for AprioriAll is the \textbf{transformation phase} in which all non-litems are dropped from the sequence database to speed up the subsequent sequence phase. This is not viable for Apriorirep1i because of its use of occurrence sets: if litems are omitted, the indices refering to occurrences don't match anymore. An approximation of this effort is made by truncating the sequences that do not contain litems. This way the sequence keys ($i$ in $s_i$) in the occurrence set still refer to the correct sequences in the sequence database and those irrelevant sequences are removed from memory. The result of applying Apriorirep1i's transformation phase is that $s_6$ is truncated. The resulting sequence database is from here on out denoted as \Dt. \\
 
 % Sequence phase
 The focus now shifts to finding the desired patterns. This is called the \textbf{sequence phase}.
 The algorithm enters into a loop, incrementing the pattern length $k$ at each iteration and continuing while there are lpatterns found of length $k-1$. Explaining the steps of this phase is easier when we skip two $k$. The occurrence set $L_3$ is shown in \cref{tab:apriorirep1i:re:L3}.
  \begin{table}[h]\centering\bgroup\def\arraystretch{1.3}
 	\caption[Intermediate result of Apriorirep1i after two iterations (running example)]{The intermediate result $L_3$ after two iterations of Apriorirep1i with our running example}
 	\label{tab:apriorirep1i:re:L3}
 	\begin{tabular}{cllll}
 		\hline
 		\tsi\ & \tblocc{abc}  & \tblocc{bca}  & \tblocc{cab}  & \tblocc{bcd}   \\ \hline\hline
 		$s_2$ & $\{0, 3\}$    & $\{1, 4\}$    & $\{2, 5\}$    &                \\
 		$s_3$ & $\{2, 5, 8\}$ & $\{0, 3, 6\}$ & $\{1, 4, 7\}$ &                \\
 		$s_4$ & $\{0, 4, 8\}$ &               &               & $\{1, 5, 9\}$  \\
 		$s_5$ & $\{0, 5, 9\}$ &               &               & $\{1, 6, 10\}$ \\ \hline
 	\end{tabular}\egroup
 \end{table}
 	% get_next_patterns
	First all candidates of length $k=4$ must be found. This step traverses all lpatterns in $L_{k-1}$ and returns an occurrence set of patterns of length $k=4$ found in \Dt. This is done by looking at each pattern's occurrence and considering the element that directly follows the pattern.  The candidates found in our running example are \spattern{abca}, \spattern{abcd}, \spattern{bcab}, \spattern{bcda}, \spattern{bcdb} and \spattern{cabc}. Again, the \texttt{purge\_nonlpatterns} step is applied to discard irrelevant occurrences. The result is shown in \cref{tab:apriorirep1i:re:seq:get_next_patterns}. This does not include all candidates, since not all of them are lpatterns.
	\begin{table}[h]\centering\bgroup\def\arraystretch{1.3}
	 	\caption[Intermediate result of Apriorirep1i with our running example after \texttt{get\_next\_patterns} step of sequence phase]{The intermediate result $L_4$ of Apriorirep1i with our running example after the \texttt{get\_next\_patterns} step of sequence phase, with \texttt{purge\_nonlpatterns} applied}
	 	\label{tab:apriorirep1i:re:seq:get_next_patterns}
	 	\begin{tabular}{clll}
	 		\hline
	 		\tsi\ & \tblocc{abca} & \tblocc{abcd} & \tblocc{bcab} \\ \hline\hline
	 		$s_2$ & $\{0, 3\}$    &               & $\{1, 4\}$    \\
	 		$s_3$ & $\{2, 5\}$    &               & $\{0, 3, 6\}$ \\
	 		$s_4$ &               & $\{0, 4, 8\}$ &               \\
	 		$s_5$ &               & $\{0, 5, 9\}$ &               \\ \hline
	 	\end{tabular}\egroup
	\end{table}

	% purge_subpattern_cycle_start
	As one might notice, \spattern{abca} is a superpattern of \spattern{abc}. Step \texttt{purge\_subpattern\_cy\-cle\_start} removes occurrences of \spattern{abca} if it links two occurrences of the subpattern \spattern{abc}. Index 0 in $s_2$ qualifies for the purge, but index 3 does not: subpattern \spattern{abc} has no occurrence at index 6 as the sequence ends after index 7. The result is shown in \cref{tab:apriorirep1i:re:seq:purge_subpattern_cycle_start}. Only the occurrences of \spattern{abcd} remain after \texttt{purge\_nonlpatterns} is applied again.
 	\begin{table}[h]\centering\bgroup\def\arraystretch{1.3}
		\caption[Intermediate result of Apriorirep1i with our running example after \texttt{purge\_subpattern\_cycle\_start} step of sequence phase]{The intermediate result $L_4$ of Apriorirep1i with our running example after the \texttt{purge\_subpattern\_cycle\_start} step of sequence phase}
		\label{tab:apriorirep1i:re:seq:purge_subpattern_cycle_start}
		\begin{tabular}{clll}
			\hline
			\tsi\ & \tblocc{abca} & \tblocc{abcd} & \tblocc{bcab} \\ \hline\hline
			$s_2$ & $\{3\}$       &               & $\{4\}$       \\
			$s_3$ & $\{\}$        &               & $\{6\}$       \\
			$s_4$ &               & $\{0, 4, 8\}$ &               \\
			$s_5$ &               & $\{0, 5, 9\}$ &               \\ \hline
		\end{tabular}\egroup
	\end{table}

% Maximal phase
Apriorirep1i places the next phase, the \textbf{maximal phase}, partially inside and partially outside of the loop, contrary to only outside as AprioriAll does. Since this phase focuses on patterns of length $k-1$, this design choice allows for better comprehensibility. The maximal phase removes occurrences of patterns that are not maximal, i.e. that are found in other patterns. First it discards patterns of length $k-1$ if they are smaller than the minimum pattern length. 

	% purge_subpatterns	 
	If $k-1 \geq minpatlen$, it applies the \texttt{purge\_subpatterns} step. \Cref{tab:apriorirep1i:re:maximal:input} shows the input for this step limited to $s_4$ and $s_5$. It shows clearly that all occurrences of \spattern{abc} and \spattern{bcd} are part of the occurrences of \spattern{abcd}. These are found by looking at the two subpatterns obtained by removing the head (resulting in \spattern{bcd})and tail (resulting in \spattern{abc}) elements, respectively, and comparing their occurrences with those of the superpattern. In this case, all occurrences of \spattern{abc} and \spattern{bcd} are discarded.

	\begin{table}[h!]\centering\bgroup\def\arraystretch{1.3}
	 	\caption[Running example input for Apriorirep1i's maximal phase reduced to $s_4$ and $s_5$]{The running example input for the maximal phase at $k=4$ of Apriorirep1i reduced to $s_4$ and $s_5$}
	 	\label{tab:apriorirep1i:re:maximal:input}
	 	\begin{tabular}{clll}
	 		      & \multicolumn{1}{c|}{$L_4$} & \multicolumn{1}{c}{$L_3$} & \multicolumn{1}{c}{ } \\ \hline
	 		\tsi\ & \tblocc{abcd}              & \tblocc{abc}              & \tblocc{bcd}          \\ \hline\hline
	 		$s_4$ & $\{0, 4, 8\}$              & $\{0, 4, 8\}$             & $\{1, 5, 9\}$         \\
	 		$s_5$ & $\{0, 5, 9\}$              & $\{0, 5, 9\}$             & $\{1, 6, 10\}$        \\ \hline
	 	\end{tabular}\egroup
	\end{table}
 
	% purge_nonlpatterns
	Finally, the maximal phase concludes with applying \texttt{purge\_nonlpatterns} again. Placing this inside the loop applies the step more often on smaller datasets, while placing it outside the loop applies it one time on the result. We've chosen the same as with AprioriAll: outside the loop. \\
	
% Result
The result of running the basic version of Apriorirep1i on our running example is shown in \cref{tab:apriorirep1i:re:maximal:output}. This shows that the occurrences of the individual items of \spattern{abc}, \spattern{bca} and \spattern{cab} have overlap and is discussed in \cref{subsec:apriorirep1iextension}.
\begin{table}[h!]\centering\bgroup\def\arraystretch{1.3}
	\caption[Result of basic Apriorirep1i applied on the running example]{The result of basic Apriorirep1i applied on the running example}
	\label{tab:apriorirep1i:re:maximal:output}
	\begin{tabular}{cllll}
		\hline
		\tsi\ & \tblocc{abcd} & \tblocc{abc}  & \tblocc{bca}  & \tblocc{cab}  \\ \hline\hline
		$s_2$ &               & $\{0, 3\}$    & $\{1, 4\}$    & $\{2, 5\}$    \\
		$s_3$ &               & $\{2, 5, 8\}$ & $\{0, 3, 6\}$ & $\{1, 4, 7\}$ \\
		$s_4$ & $\{0, 4, 8\}$ &               &               &               \\
		$s_5$ & $\{0, 5, 9\}$ &               &               &               \\ \hline
	\end{tabular}\egroup
\end{table}



\begin{comment}
Structure!

Use a paper-like approach, for now based on Barreto (2014):
	- background (follow the introductions of Barreto (2014) and Hu (2015)):
		- what is sequential pattern mining
			Agrawal (1995) was the first to define the problem of sequential pattern mining. He based this off of the use case of customer transactions. Table ... shows the format of such a dataset. This 
		- sequential databases
		- application areas
		- approaches (apriori (pros/cons) and prefixspan (pros/cons)). maybe look for a review paper.
			- explain the introduction of minimum repetition support (Barreto)
    - my problem and why other solutions don't work
		- explain why related work does not work for me: repetitive is not the same as cyclic: cyclic implies a constant distance between repetitions, and periodicity.
    - my algorithm
    	- start with an example dataset
    		- dev is on Zeppelin
    	- note that the algorithm was not optimized for performance.
    	- explain each step and what goes wrong when the step is omitted
    	- add some metrics about performance (i.e. my situation).

\end{comment}


\subsection{Apriorirep1i (complete)}
\label{subsec:apriorirep1iextension}

In the sequence \seq{abcabcabc} it is clear that pattern \spattern{abc} is the main sequential pattern. Sequence \seq{bcabcabca} clearly has the main sequential pattern \spattern{bca}. In a sequence database we should take them both into account, seeing as they both contain the other's main sequential pattern. This is what we've called \textbf{self-succeeding pattern origin ambiguity}: which pattern should be considered the main pattern? An imperfect solution was found using a sliding window approach.

	% purge_cyclic_shifts
	This is mainly dealt with by the \texttt{purge\_cyclic\_shifts} step. We take the iteration $k=3$ to illustrate. Traversing $s_2$, it starts the sliding window at index 0 and checks in $L_k$ if there's a frequent pattern with an occurrence there. It finds \spattern{abc} and remembers it as the main pattern. Incrementing the index to 1, it checks if the cyclically shifted version \spattern{bca} is a frequent pattern occurring there. This is the case, so it purges that occurrence of \spattern{bca}. When the occurrence of the main pattern is traversed (after index 2), it forgets the main pattern and starts looking for a new one. Continuing with $s_3$ the first main pattern found is \spattern{bca} and does the same. \Cref{tab:apriorirep1i:re:seq:purge_cyclic_shifts} shows the result of this step. It shows that it indeed discarded \spattern{abc} from $s_3$, \spattern{bca} from $s_2$ and \spattern{cab} from both $s_2$ and $s_3$.
	\begin{table}[h!]\centering\bgroup\def\arraystretch{1.3}
		\caption[Intermediate result of Apriorep1i after \texttt{purge\_cyclic\_shifts} step of sequence phase]{The intermediate result $L_3$ of Apriorep1i after \texttt{purge\_cyclic\_shifts} step of sequence phase with our running example}
		\label{tab:apriorirep1i:re:seq:purge_cyclic_shifts}
		\begin{tabular}{cllllll}
			\hline
			\tsi\ & \tblocc{abc}  & \tblocc{bca}  & \tblocc{bcd}   & \tblocc{cab} &  &  \\ \hline\hline
			$s_2$ & $\{0, 3\}$    & $\{\}$        &                & $\{\}$       &  &  \\
			$s_3$ & $\{\}$        & $\{0, 3, 6\}$ &                & $\{\}$       &  &  \\
			$s_4$ & $\{0, 4, 8\}$ &               & $\{1, 5, 9\}$  &              &  &  \\
			$s_5$ & $\{0, 5, 9\}$ &               & $\{1, 6, 10\}$ &              &  &  \\ \hline
		\end{tabular}\egroup
	\end{table}

	% purge_interconnecting_subpatterns
	There is one additional step required as a consequence of the sliding window approach. Pattern \spattern{ca} is also recognized as frequent due to the fact that it connects self-succeeding occurrences of \spattern{abc}. In $s_2$, this results in occurrences at indices 2 and 5. The step \texttt{purge\_interconnecting\_subpatterns} identifies such cases by comparing occurrences of frequent subpatterns with the tail-less cyclically shifted versions of each pattern, e.g. subpatterns \spattern{ab}, \spattern{bc} and \spattern{ca} of \spattern{abc}. In our example, it retains index 5 of $s_2$ because no occurrence of \spattern{abc} is found at corresponding index 6. \\

Note that the result (shown in \cref{tab:apriorirep1i:re:maximal:outputcomplete}) of running Apriorirep1i on our example ultimately does not include occurrences of \spattern{abc} and \spattern{bca}. This is because \spattern{abc} is a subpattern of \spattern{abcd} in $s_4$ and $s_5$ and is thus discarded by the \texttt{purge\_subpatterns} step. The remaining occurrences of \spattern{abc} in $s_2$ and \spattern{bca} in $s_3$ both do not adhere to the minsupport requirement. This interplay of side effects is less significant when dealing with large datasets.
\begin{table}[h!]\centering\bgroup\def\arraystretch{1.3}
	\caption[Result of complete Apriorirep1i applied on the running example]{The result of complete Apriorirep1i applied on the running example}
	\label{tab:apriorirep1i:re:maximal:outputcomplete}
	\begin{tabular}{cl}
		\hline
		\tsi\ & \tblocc{abcd} \\ \hline\hline
		$s_4$ & $\{0, 4, 8\}$ \\
		$s_5$ & $\{0, 5, 9\}$ \\ \hline
	\end{tabular}\egroup
\end{table}


\subsection{Application}
\label{subsec:apriorirep1iapplication}

Apriorirep1i expects a sequence database as input. This was done by simply mapping each action (as described in \cref{sec:dataprep:repetitivetasks}) to a symbol. After the algorithm was applied on the log entries dataset for each NPS response, the results from the occurrence set were aggregated into single values corresponding to \cref{df:rep:pat,df:rep:iter,df:rep:weighted} and stored for the next phase of this project. Additionally, a dataset with the found patterns was constructed to support Topicus with future analysis of the repetitive patterns.

The requirement in terms of efficiency was that it could run in an acceptable time frame. There were 1046 NPS entries, 107 355 sessions and 6 320 136 actions in total. Averaging the average amount of actions per session over the amount of NPS responses, the result is 61.79 actions per session. Apriorirep1i took about 15 minutes to run on all of the log entry datasets of all NPS responses.


\subsection{Related work}
\label{subsec:apriorirep1i:relatedwork}

Toroslu introduced the notion of minimum repetition support~\cite{Toroslu2003}. He called this the field of \textit{cyclic} pattern mining, a generalization of sequential pattern mining. It is a generalization because a minimum repetition support of 1 would yield the same results as sequential pattern mining algorithms. Toroslu presented an algorithm similar to Apriorirep1i.
His algorithm is also based on Agrawal and Srikant's AprioriAll~\cite{Agrawal1995}. Contrary to sequential pattern mining algorithms and like Apriorirep1i, it finds only patterns with itemsets of length 1. Instead of our occurrence set structure, it uses a hash-tree for efficient storage of candidates. Where Apriorirep1i only finds patterns with consecutive elements, Toroslu's algorithm allows patterns to be interleaved with other elements. This is more appropriate for the use cases of his research: the stock market and customer transactions. To illustrate, consider the sequence \seq{cacbacacabbacabacaac}. One of the cyclic patterns with repetition support 3 that Toroslu identifies is \spattern{aba}.

Another important difference is how Toroslu deals with his equivalent problem to the self-succeeding pattern origin ambiguity. In his case the patterns are not (necessarily) self-succeeding, as patterns can be interleaved with other elements. Terminology aside, the pattern origin ambiguity is still present and Toroslu takes a different approach. He views the shifted versions of a pattern as members of the same family. Consider the previously mentioned sequence again. The shifted patterns \spattern{aab}, \spattern{aba} and \spattern{baa} have repetition supports 2, 3 and 3, respectively. All three patterns are considered supported in the sequence (assuming a minimum repetition support of 3) because the family's highest repetition support (\spattern{aba} or \spattern{baa}) satisfies the minimum. The nature of these cyclic patterns ensure that the difference between the repetition support of family members is only 1. To illustrate both approaches in the context of this study, take the sequence \seq{cabcabcghabcabc}. Apriorirep1i's basic version would recognize all occurrences of both the \spattern{cab} and \spattern{abc} patterns. Its sliding window approach of dealing with the ambiguity would identify \spattern{cab} as the main pattern of the first six indices and thus remove the first two found occurrences of \spattern{abc}. Its result would be repetition support 2 for both \spattern{cab} and \spattern{abc} patterns. Toroslu's approach would find repetition support 4 for pattern \spattern{cab}: the first two and the last one are obvious; the third occurrence is interleaved with the elements \spattern{gh}. Applying Toroslu's approach to Apriorirep1i can be easily done by joining occurrences of shifted patterns into one family. \Cref{tab:apriorirep1i:re:maximal:output} illustrates that this would work: the shifts of each pattern's elements correspond to the shifts in their occurrence index, e.g. \spattern{bca} occurs one index later than \spattern{abc}. \\

Other related work focuses on approaches with more or different requirements. Hu and Chiang~\cite{Hu2011}, who extended the work of Toroslu, state that a constant minimum repetition support is too coarse-grained to take all types of sequential events into account. They introduce multiple minimum repetition supports and implement this in their PrefixSpan-based algorithm rep-PrefixSpan. PrefixSpan only takes the first occurrence of a pattern as a prefix. Rep-PrefixSpan considers each occurrence of a pattern as a prefix and limits the projected database length with another parameter. It does not deal with the ambiguity mentioned above. Rep-PrefixSpan is less suitable for datasets where the elements are unknown beforehand, as the researcher has no idea which element should have which minimum repetition support. \\

Barreto and Antunes~\cite{Barreto2014} also use a PrefixSpan-based approach. They state that periodicity is relevant for cyclic patterns. They define a pattern as a ($s$, $\rho$, $\delta$) tuple, where $s$ is the (\textbf{contiguous}) sequence, $\rho$ is the period (i.e. the space between two occurrences) and $\delta$ is the amount of repetitions. Although their algorithm, PrefixSpan4Cycles, aims to find periodic patterns, its first part finds cyclic patterns without considering their periodicity. It does this in a way highly similar to basic Apriorirep1i. The data structure used is almost identical to occurrence sets. The biggest difference is that PrefixSpan4Cycles is recursive (depth-first), while Apriorirep1i is iterative (breadth-first). It also supports itemsets of lengths greater than 1, which Apriorirep1i does not as this was not relevant for our use case.\\

In conclusion, Apriorirep1i's basic part should have been subject to Barreto and Antunes' approach of the first part of PrefixSpan4Cycles. Apriorirep1i's ambiguity issue can be solved by the familial approach provided by Toroslu. Combining these methods and doing measurements of effectiveness and efficiency is considered future work into this algorithm for finding repetitive patterns that consist of consecutive actions and without the requirement of periodicity.





%----------------------------------------------------------------------------------------
%	SECTION 3
%----------------------------------------------------------------------------------------
\section{Low-hanging fruit}
\label{sec:patternextraction:lhf}

The pattern extraction operations for \cref{df:school,df:edulevels,df:npsage} are relatively simple.
Each user can have multiple schools at which he's worked in the 90 days prior his NPS response. This most often occurs because schools fuse, according to Topicus experts. The data format for \creffulldf{df:school} should thus be binary, where each school is a field. The resulting (reduced) dataset format is shown in \cref{tbl:patext:goal:lhf:school}. The source is the mapping data mentioned in \cref{sec:dataprep:lhf}.
\begin{table}[h!]\centering\bgroup\def\arraystretch{1.3}
	\caption{The reduced dataset format for \cref{df:school}}
	\label{tbl:patext:goal:lhf:school}
	\begin{tabular}{llp{6cm}}
		\hline
		Field             & Data type & Description                                                                          \\ \hline\hline
		\field{NPSID}     & string    & The NPS ID.                                                                          \\
		\field{school: *} & boolean   & Whether the user was associated with the school mentioned in the field ('*') or not. \\ \hline
	\end{tabular}\egroup
\end{table}

A similar line of reasoning is used for \creffulldf{df:edulevels}. Each user can teach multiple education levels, so the target data format should be binary. \Cref{tbl:patext:goal:lhf:edulevels} shows the reduced format. Again, the source is the mapping data previously mentioned in \cref{sec:dataprep:lhf}.
\begin{table}[h!t]\centering\bgroup\def\arraystretch{1.3}
	\caption{The reduced dataset format for \cref{df:edulevels}}
	\label{tbl:patext:goal:lhf:edulevels}
	\begin{tabular}{llp{6cm}}
		\hline
		Field            & Data type & Description                                                                      \\ \hline\hline
		\field{NPSID}    & string    & The NPS ID.                                                                      \\
		\field{edlvl: *} & boolean   & Whether the user taught the education level mentioned in the field ('*') or not. \\ \hline
	\end{tabular}\egroup
\end{table}

Finally, feature \creffulldf{df:npsage} has a numeric format and the format is shown in \cref{tbl:patext:goal:lhf:npsage}. The source is the date of the NPS response, included in the NPS dataset.
\begin{table}[h!t]\centering\bgroup\def\arraystretch{1.3}
	\caption{The dataset format for \cref{df:npsage}}
	\label{tbl:patext:goal:lhf:npsage}
	\begin{tabular}{llp{6cm}}
		\hline
		Field          & Data type & Description                                                          \\ \hline\hline
		\field{NPSID}  & string    & The NPS ID.                                                          \\
		\field{npsage} & integer   & The age of an NPS response measured from Janaury 1st, 2016, in days. \\ \hline
	\end{tabular}\egroup
\end{table}
