\chapter{Conclusion}
\label{chp:conclusion}
% http://www.postgraduate.uwa.edu.au/__data/assets/file/0004/2906428/Thesis-Structure_JE180716.pdf
% http://sokogskriv.no/en/writing/structure/structuring-a-thesis/
% https://www.grad.ubc.ca/current-students/dissertation-thesis-preparation/structure-theses-dissertations
% https://www.gcu.ac.uk/library/smile/writingandnumeracy/executivesummaryandconclusion/theconclusion/

With this study we've tried to find patterns in teachers' usage data with which NPS responses can be predicted regarding the education support application \somtoday. \Cref{sec:conclusion:findings} presents our findings, and \cref{sec:conclusion:discussion} discusses those. Finally, \cref{sec:conclusion:recommendations} points out this thesis' contributions and argues our recommendations.

%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------
\section{Main research findings}
\label{sec:conclusion:findings}

\did{Summary mentioning answers to research questions.}
\Cref{chp:featurediscovery,chp:dataprep,chp:patternextraction,chp:modeltraining,chp:validation} have answered the research questions and each one discusses a distinct phase of this project:

% feature discovery
The feature discovery phase had the purpose of getting an indication of which usage patterns might be of influence on the NPS response. A list of 25 data features was created by talking to the system's users and domain experts and by looking at the qualitative data. Ten of those data features (\ref{df:rep:pat} to \ref{df:rep:weighted} and \ref{df:prev:total} to \ref{df:npsage}), divided over the categories system usage, repetitive tasks and low-hanging fruit, were selected from this list to use in this project. Selection was done based on the features' expected impact on the NPS response, their preparation time and their generalizability to other systems.

% data preprocessing
The data preprocessing phase focused on transforming the raw data into a format that was usable for research. This was done by applying several filters (e.g. based on user role, time, relevance and duplicity) and ETL operations (e.g. grouping into sessions and including the timestamp of the next log entry).

% pattern extraction
The pattern extraction phase found the usage patterns for each user and compressed them into one value per data feature, resulting in ten values per NPS response. This was the most complex with the repetitive tasks category, as we initially could not find an algorithm in scientific literature that met our requirements. The algorithm Apriorirep1i was created to find repetitive tasks from which, in turn, values for \cref{df:rep:pat,df:rep:iter,df:rep:weighted} were extracted. The system usage features (\ref{df:prev:total} to \ref{df:prev:total:switch}) were extracted by looking at the time between subsequent requests of the same user and the low-hanging fruit features were extracted from the NPS dataset (\ref{df:npsage}) and \somtoday's database (\ref{df:school} and \ref{df:edulevels}).

% model training
The model training phase tried to find predictive value in the extracted patterns by applying a brute force approach. A multitude of combinations of models, model parameters an preprocessing methods was tried. The most significant findings are aggregated in \cref{tab:conclusion:results}. This shows that even the best performing models are doing only slightly better than the scenario where the dominant class is always predicted. Applying a model with such performance in a production environment would yield results that are unreliable. Even though the three-value categorical case has a 10.23\% accuracy improvement, the coarse granularity of the target attribute diminishes its significance. Thus, there is practically no predictive value in the dataset. 

% validation
The validation phase had the same outcome. This was determined by validating that the model training workflow was correct and by looking at decision trees and other performance metrics. The low feature-to-NPS correlations shown in \cref{tab:attrcorr} support this.

\begin{table}[h!]
	\centering\resizebox{1.0\textwidth}{!}{%
	\begin{threeparttable}
		\centering
		\caption{Top results from \cref{sec:modeltraining:results} put together}
		\label{tab:conclusion:results}
		\bgroup\def\arraystretch{1.2}
		\begin{tabular}{p{1.9cm}p{1.8cm}lp{3.7cm}ll|ll}
			\hline
			\nl{target\\attribute}  & \nl{input\\attributes} & model & model parameters                                                      & accuracy & MAE    & accuracy\tnote{*} & MAE\tnote{*} \\ \hline\hline
			categorical (11 values) & numerical, binary      & RF    & {\small number of trees:~21, criterion:~gain ratio}                   & 27.92\%  &        & 27.63\%           &              \\
			categorical (3 values)  & categorical, binary    & kNN   & {\small k:~43, weighted vote: true, nominal measure: NominalDistance} & 54.30\%  &        & 44.07\%           &              \\
			numerical               & numerical              & SVM   & {\small kernel type:~anova, C:~0}                                     &          & 1.7267 &                   & 1.7333       \\ \hline
		\end{tabular}\egroup
		\begin{tablenotes}
			\item[*] {\footnotesize If the dominant class is always predicted.}
		\end{tablenotes}
	\end{threeparttable}}
\end{table}

\did{Answer to main research question.}
% conclusion
These answers to the research questions lead to the answer to the main research question: \textbf{teachers' usage data can not be reliably used to predict the user's loyalty towards the system.} At least, not based on the chosen data features and dataset.


%----------------------------------------------------------------------------------------
%	SECTION 2
%----------------------------------------------------------------------------------------
\section{Discussion}
\label{sec:conclusion:discussion}

The answer to the main research question raises a new question: why can't the usage data be used to predict user loyalty? We expected to find at least \textit{some} predictive value. Three themes were identified in which to look for an explanation. \\

 \did{Points of discussion in the process.}
 The first is \textbf{the process} of going from the raw data to the NPS prediction. 
  We skip the feature discovery phase for now, as this will be discussed in our recommendations. \par
  In the context of data preprocessing more time could've been spent on getting to know the ins and outs of the (usage data) dataset, e.g. how each action or set of actions in the system is mapped in the dataset. With our approach we've done our best, for example, to discard all irrelevant log entries and only keep 'actionable' entries, but more irrelevant entries can probably be found if more time is spent on it. \par
  In the pattern extraction phase, the Apriorirep1i algorithm can be improved with a better solution to the self-succeeding pattern origin ambiguity. One way is to extend the sliding window approach so that it also looks at the tail of a sequence of self-succeeding pattern occurrences, e.g. by detecting that sequence \seq{abcabcabc} begins \textit{and} ends with pattern \spattern{abc}. A different and preferred method is the approach used by Toroslu: considering a pattern's shifted versions (e.g. \spattern{abc}, \spattern{bca} and \spattern{cab}) as members of the same family and consequently dealing with pattern families instead of individual patterns. This would result in a better selection of repetitive tasks. Had our current methods found a hint of predictive value in the repetitive tasks data features, we would have reason to believe that these improvements to Apriorirep1i could result in better predictive performance. However, this is not the case, leading us to believe that such a scenario would not change the conclusions of this project. \par
  Regarding the model training phase, we've tried to cover the possibility of using different models, model parameters and preprocessing methods with our brute force approach. As a result, we have no specific points of discussion for this phase.
  \\
  
 \did{Points of discussion in the target data.}
 The second theme is \textbf{the target data}: the NPS dataset. 
  The class imbalance was evident during the model training phase. It is possible that a better distribution of NPS responses would've increased the model's predictive performance. This is unlikely, seeing as our efforts of class upsampling, downsampling and SMOTE upsampling did not yield better results. \par
  A different and more Occam's razor-like explanation is that, in addition to the metric's debatable validity mentioned in \cref{sec:background:nps}, NPS responses simply can't be predicted based on usage data. Because, for example, the NPS metric might be too coarse-grained and global to let behavioral patterns influence it. Perhaps NPS responses can only, or primarily, be predicted based on non-usage data, such as attitude, intrinsic motivations, general happiness or IT competence. The only way to be reasonably confident that this is the case is by researching more usage patterns. \\
  
 \did{Points of discussion in the source data.}
 The third theme is \textbf{the source data}: the log entries dataset.
  One way of looking at this is by applying the previous explanation to the dataset used in this project: there might indeed be no predictive value in \textit{our chosen features}. This can also be tested by researching more usage patterns. \par
  Another perspective shows that our familiarity with the dataset is limited to what was necessary for its analysis. It is possible that our specific knowledge hindered us from processing the dataset properly. We may have not taken into account nuances that we were unaware of. For example, why is there such a high variation in the amount of log entries per NPS response? And are the repetitive tasks found by Apriorirep1i indeed repetitive tasks, or can they be explained by something we haven't thought of? We have talked to domain experts, but this is merely an approximation an approximation to full familiarity with the data. If our knowledge had been all-encompassing, we'd be able to answer these kinds of questions. This understanding might have lead to better data preprocessing and pattern extraction, which in turn might have lead to better results. One way to deal with this is addressed in our recommendations.

%----------------------------------------------------------------------------------------
%	SECTION 3
%----------------------------------------------------------------------------------------
\section{Recommendations}
\label{sec:conclusion:recommendations}

% relevance/contributions
 % science
 \did{Relevance/contributions to Topicus and to science.}
 Our contributions to Topicus and to science in general are largely similar.
 We looked into   making first steps   for creating a non-invasive, automated and userbase-wide way   for system owners to discover which users to focus their improvement efforts on    in the context of education support web applications.
 As our conclusion is that this is not feasible with our current research parameters, we contribute the notion to redirect focus to different data features.
 We provide Topicus with a list of data features that can be researched next. Likewise, the scientific community can use this list as a starting point for research with similar systems.
 We also provide Topicus with the process we used for data preprocessing. This validated workflow could save valuable amounts of time for future research efforts.
 In addition we have provided Topicus with the repetitive patterns found in the pattern extraction phase. These can be used for further analysis, for example to determine which tasks are repeated the most and thus can be considered for batching.
 
 Although Apriorirep1i is not yet ready for practical use, it contributes to the field of data mining and specifically sequential pattern mining. Together with the suggestion of combining parts of the algorithms introduced by Toroslu in 2003 and Barreto and Antunes in 2014, it opens the door for efficient analysis into repetitive tasks consisting of consecutive actions and without the requirement of periodicity. Future work includes developing this improvement and doing measurements into its effectiveness and efficiency.
 \\

% recommendations
 % Topicus
 \did{Recommendations to Topicus.}
 The motivations for this research are well-founded and because the conclusion definitely does not rule out the possibility that user loyalty towards a system can be predicted based on their behavior, our main recommendation for Topicus is to focus on finding predictive value in other usage patterns. From the list compiled in our feature discovery phase, we'd recommend starting with the features about clickstreams (\ref{df:cs:avglength} \cmmnt{\ref{df:cs:avgduration}} to \ref{df:cs:avgpersession}) and encountered downtimes (\ref{df:downtimes}). These can be researched in a cost-effective way, as they are fairly straightforward. Additionally, methods for analyzing clickstreams are abundandly researched by the scientific community. \par
 Should Topicus indeed decide to search for predictive value in more usage patterns, then valuable time can be saved by improving their data infrastructure beforehand. To illustrate: one of Apache Drill's advertised advantages, its schema-free flexibility, considerably hindered our preprocessing efforts. Even though a strict schema might require a bit more effort to set up, it does allow for easier data analysis. The collection and storage of log data should facilitate in future data analysis. Our recommendation (to data-processing organizations in general) is to make sure the process of data analysis is actually assisted by the choice of collection and storage methods and not obstructed it. \\
 
 % science
 \did{Recommendations to scientific community.}
 Finding predictive value in more usage patterns should also be the goal for future work of the scientific community. There is, however, one concern with this project from a scientific standpoint: the indicative nature of the feature discovery results. Our recommendation is to conduct a more in-depth feature discovery research project. Although it is not primarily recommended to Topicus due to its extensiveness and the indirectness of its contribution to NPS predictions, it does provide value and is worth considering by Topicus. A full-scale user experience study with the goal of usage data analysis gives the researcher quantitative and qualitative data about what users think of different aspects (e.g. navigation and layout) and functionalities and how their behavior is mapped onto the log entries dataset. An extensive survey is one way, and should at least be supplemented by sit-in sessions where the researcher observes the user's behavior and the generated data in real-time. Having better insight leads not only to validated feature selection, it also leads to refined knowledge about how data features can be measured and what the nuances are. Choosing this approach, one or multiple directed studies into specific usage patterns can be set up and the researcher has a better chance of finding predictive value in user behavior: the researcher is no longer looking in the proverbial dark.
 
 \did{Cool closing statement.}
 Data is a basis for research, so it holds that, especially in the context of data research: knowledge\footnote{ about the subtleties of the researched data} is power.