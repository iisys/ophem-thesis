\chapter{Data preprocessing}
\label{chp:dataprep}
This phase is said to take up a majority of the time spent on a data analysis project~\cite{Hussain2010}, as was indeed the case with the present study. The efforts put into the data preprocessing and its associated validation turned out to be essential. During validation, several bugs in the workflow and data infrastructure were uncovered. The steps taken, issues encountered and a basic analysis of the results are discussed in this chapter. \Cref{sec:dataprep:nps,sec:dataprep:logs} cover the NPS and request log datasets, respectively, while \cref{sec:dataprep:usage,sec:dataprep:repetitivetasks} describe feature-specific efforts. Note that some efforts are similar and the preprocessed datasets could be combined to save storage space. However, seeing as storage space was no issue, a clear separation was preferred.


%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------
\section{NPS}
\label{sec:dataprep:nps}
% Goal.
The desired dataset format is listed in \cref{tab:dataprep:nps:goal}.

\begin{table}[h!]
	\centering\bgroup\def\arraystretch{1.2}
    \caption{Target format for the NPS dataset}
    \label{tab:dataprep:nps:goal}
    \begin{tabular}{llp{9cm}}
    	\hline
    	Field   & Data type & Description                                                                                                                                                                                                                      \\ \hline\hline
    	ID      & string    & An identifier for the NPS response.                                                                                                                                                                                              \\
    	date    & date      & The date the NPS response was entered.                                                                                                                                                                                           \\
    	score   & integer   & The actual NPS response, being $0 \leqslant \emph{score} \leqslant$ 10.                                                                                                                                                          \\
    	user ID & string    & An identifier for the user. This is used to join this dataset with the logs. This includes the username, organization and organization abbreviation, seeing as the \napi\ and \api\ logs utilize distinct combinations of those. \\ \hline
    \end{tabular}\egroup
\end{table}

% Source.
The NPS dataset (discussed in \cref{subsec:intro:availabledata:nps}) was provided by Topicus in Microsoft Excel's XLSX format. The data integration suite \servicelink{Pentaho Kettle/Spoon}{http://community.pentaho.com/projects/data-integration/} was used to apply some basic ETL operations: removal or renaming of columns, special character removal, string trimming and saving in the simple comma-separated value (CSV) format. The CSV file was then uploaded via Apache Ambari to the distributed storage, making it available to the Apache Zeppelin workspace.

% Filter.
Subsequently, Drill's SQL implementation was utilized to apply some more ETL operations and store the dataset in Drill's native Parquet format. The first priority was to only keep the NPS responses of teachers. This was done using two filters.
\begin{description}
    \item[Prior process: \textit{active account employee}]
        Only the value \textit{active account employee} was kept of the prior process field. Teachers are always employees, so students and parents are discarded. By disregarding non-'\textit{active account}' values such as \textit{service desk} and \textit{training}, an attempt is made to discard NPS responses that were given in the context of a specific situation.
    \item[Roles: \textit{teacher} and \textit{mentor}]
        The filter for the role of \textit{teacher} is obvious. Nearly all mentors are also teachers, so this role was also taken into account. The few cases in which this is not the case (e.g. a dean might be a mentor, but not a teacher), are negligible according to domain experts at Topicus. Discarded roles included \textit{application administrator} and \textit{miscellaneous}.
\end{description}

Another filter is applied on the dates of the NPS responses. For the log dataset, the start date was chosen on which all schools in the country have started (seeing as this differs between regions): September 4th, 2016. This dictates the NPS date constraint of December 3rd, 2016, because 90 days of log data is analyzed for each NPS response. The maximum date is set on June 30th, 2017, as this is the end of the academic year. The system will be utilized after that date, but to normalize the data as much as possible this has not been taken into account.

The next step was making the join with the request log dataset possible. Individual log entries include a user's username and organization, while the NPS dataset includes user identification strings (\textit{UUID}s) generated by \somtoday. Topicus has provided an XLSX file containing a mapping of UUIDs to usernames and organisations. This dataset was made available to Drill using the same workflow as used with the NPS dataset. Using this mapping, each NPS response was joined with the associated username and organization, enabling future joins with the log dataset. Each NPS response also received a unique ID. \\


% Capitalization.
There were still a few inconsistencies found during validation in the record counts between these datasets. In some cases, different capitalization was used in organization names or usernames. These issues were resolved by lowercasing all of these values after confirming with Topicus that the capitalization has nothing to do with user or organization uniqueness. 

% Multiple organizations.
A different issue was encountered that is a result of merging schools. This resulted in some users having multiple organizations. Due to the nature of upcoming joins with the log data, it was possible to simply include multiple NPS response records per user and keep the NPS response ID the same, thus the only difference being the organization. When applying a left join between the NPS and log datasets later on, the correct log entries are associated with the correct NPS response. The same issue was identified and resolved in the context of usernames.

% Multiple responses.
Finally, it also became apparent that some users had submitted a response to the NPS survey twice. Of the 1085 NPS responses, 12 (1.1\%) were from users with multiple responses: 6 users had two responses. Statistics of this subset are shown in \cref{tab:nps:multipleresponses}. The time between responses and the variance in score is motivation to treat these NPS responses as distinct data points. Note that there was one anomalous instance in which a user had just 47 days between two responses. The first was discarded: the user has more experience with the system at the time of his second response, making a well-founded response more likely. To distinguish between responses from the same user, different IDs were assigned consisting of the response date concatenated with the first eight characters of the user's UUID. Those eight characters have been checked for uniqueness.
\begin{table}[h!]
    \caption{Descriptive statistics of users with multiple NPS responses}
    \centering\bgroup\def\arraystretch{1.3}
    \label{tab:nps:multipleresponses}
    \begin{adjustbox}{center}
        \begin{tabular}{p{3.0cm}rll}
        	\hline
        	                          & \multicolumn{2}{l}{\nl{Time between subsequent\\responses}} & \nl{NPS score\\increase} \\ \hline\hline
        	Average                   & 133.5 days & $\approx$ 4.4 months                           & 0.17                     \\
        	Sample standard deviation &  45.1 days & $\approx$ 1.5 months                           & 0.98                     \\
        	Minimum                   &  84.0 days & $\approx$ 2.8 months                           & -1                       \\
        	Maximum                   & 192.0 days & $\approx$ 6.3 months                           & 1                        \\ \hline
        \end{tabular}
    \end{adjustbox}\egroup
\end{table}

The response and temporal distributions are shown in \cref{fig:dataprep:nps:respdist,fig:dataprep:nps:tempdist} and are the exact same as \cref{fig:rawdata:nps:respdist,fig:rawdata:nps:tempdist} (respectively) from \cref{subsec:intro:availabledata:nps}. They are repeated here for convenience.
\begin{figure}[h]
    \centering
    \begin{adjustbox}{center}
        \includesvg[width=1.25\textwidth]{Figures/rawdata-nps-respdist}
    \end{adjustbox}
    \caption{Response distribution of teacher NPS responses}
    \label{fig:dataprep:nps:respdist}
\end{figure}
\begin{figure}[h]
    \centering
    \begin{adjustbox}{center}
        \includesvg[width=1.25\textwidth]{Figures/rawdata-nps-tempdist}
    \end{adjustbox}
    \caption[Temporal distribution of teacher NPS responses]{Temporal distribution of teacher NPS responses. Note that the different response values are shown solely to indicate that there is variation.}
    \label{fig:dataprep:nps:tempdist}
\end{figure}

After preprocessing the usage logs it was discovered that 39 NPS respondents had no usage logs that could be associated with them. These NPS respondents were discarded, as we could not reliably determine whether the absence of usage logs could be considered part of a usage pattern. To do that, research into (the data of) all 39 respondents would show whether they explicitly made a choice not to use \somtoday\ and \somdocent\ or whether they switched accounts, only just started to use the system, or had some other reason for not generating log entries. After discarding these samples, the dataset consisted of 1046 NPS respondents.


%----------------------------------------------------------------------------------------
%	SECTION 2
%----------------------------------------------------------------------------------------
\section{Usage logs}
\label{sec:dataprep:logs}
% Goal.
The desired dataset format is listed in \cref{tab:dataprep:logs:goal}. The \napi\ and \api\ logs are treated separately due to the different level of detail included in their logs.

\begin{table}[h!]
    \centering\bgroup\def\arraystretch{1.3}
    \caption{Target format for the logs dataset.}
    \label{tab:dataprep:logs:goal}
    \begin{tabular}{p{1.8cm}p{2cm}p{9.2cm}}
    	\hline
    	Field           & Data type            & Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                          \\ \hline\hline
    	NPS ID          & string               & A reference to the NPS response that the log entry is associated with.                                                                                                                                                                                                                                                                                                                                                                                                               \\
    	timestamp       & timestamp            & The timestamp that Wicket received the request, accurate to the millisecond.                                                                                                                                                                                                                                                                                                                                                                                                         \\
    	request details & strings and integers & Various details about the page requested and the page from which the request was made. This includes the \field{response*}, \field{event*}, \field{*component*} and \field{behaviorClass} fields for the \napi\ logs, the \field{restResource}, \field{range}, \field{category} and \field{method} fields for the \api\ logs and the \field{requestedUrl} and \field{duration} fields for both log types. Specific field descriptions are available in \cref{tab:rawdata:usagelogs}. \\ \hline
    \end{tabular}\egroup
\end{table}

% Source.
As explained in \cref{subsec:intro:availabledata:usage}, the request logs are accessible to Apache Drill via the Zeppelin web interface. Using SQL queries, a new dataset was created that only includes the relevant log entries and columns.
% Filters.
To make efficient data analysis possible, several main filters had to be applied:
\begin{enumerate}
    \item Only the logs relating to \textbf{NPS responses} are relevant. This filter was applied based on the user and organization associated with the log entry, and the users in the NPS dataset.
    \item A \textbf{time constraint} is applied based on the date of the NPS response. This is an attempt to normalize the amount of log entries per NPS response, to keep the logs actually relevant to the NPS response (e.g. logs from two years prior an NPS response are expected to have next to no influence on the given NPS score) and to account for structural and functional improvements to \somtoday\ that accumulate over time. For this reason, a time period of 90 days prior to the day of the NPS response was chosen.
    \item Duplicates should be removed. The process of data aggregation described in \cref{subsec:intro:availabledata:infra} is not perfect. This has led to several instances in which data was aggregated multiple times, resulting in duplicate log entries.
\end{enumerate}

%In addition to filtering on logs relevant to specific users, the NPS response ID would have to be added to each log entry. This is to make sure grouping per NPS response can easily be done later. The way to do both in Drill is by 

% Issue: joining -> casting -> view.
The first issue encountered pertains to Drill's 'schema-free' capabilities. To apply filtering of any kind, conditional SQL statements must be possible. However, due to variation in the data formats and values, Drill often infers them inaccurately. Also, it does this for each value separately. This is useful in many situations, but in this case leads to numerous errors when applying comparisons and join operations. Drill might infer a numeric data type on a value that should be treated as a string, resulting in an error when a string data type and a 'numeric' data type are compared. This can be solved by explicitly casting each column and each value. In practice, this was applied by creating a view that returns casted values for each column, thereby effectively enforcing a schema and abandoning Drill's schema-free advantages. 

% Validation.
The next step was applying the first two filters mentioned above. In theory, this is fairly straight-forward: take the NPS dataset, use a left join with the logs on the user ID and apply a filter based on the NPS date. This method was invalidated by comparing the records counts of result sets generated by equivalent queries. By applying a step-wise approach, a solution was found. Each filter was implemented in different ways: using joins versus using conditionals with SQL's IN operator (filter 1), taking all original data as input at once versus taking subsets of the data based on their timestamp (filter 2), using SQL's SELECT DISTINCT operator versus grouping on all relevant fields (filter 3). In addition these filters were tried in different orders. Finally, the effectiveness of using a view-wrapper was validated by also trying the same without a view (but with casting).
Both choices for filter 1 performed well, except that using a join has the advantage that the NPS ID can be included immediately instead of in a later stage. Applying filter 2 resulted in the conclusion that Drill somehow did not actually include all data when querying the complete dataset (and it was also slower, seeing as the complete dataset also includes irrelevant logs from 2015). The solution was to query the years 2016 and 2017 separately. The options for filter 3 had the same result and both gave rise to memory issues. This was resolved by applying filter 3 on the intermediate result set of filters 1 and 2.
Eventually, this process of preprocessing consisted of eight queries. One extraction query that applied filters 1 and 2 (essentially extracting the relevant log entries from the original dataset) and one filter query that applied filter 3 on the result of the previous filter. These two queries are executed on each relevant year (2016 and 2017) and on both log types, which makes the sum of eight. The only non-semantical query difference between the log types is that some \api\ logs have duplicates with different \field{duration} values. This was handled by taking only the log entry with the largest \field{duration} into account in the second query. \\

% Non-page filters.
In a later stage, it became apparent that the desired dataset has another requirement: requests that are not an action of a user are irrelevant. Each of the following filters has been validated by selecting all matching requests and incrementally checking if they indeed are irrelevant. These have also been checked by data experts at Topicus.
\begin{description}
    \item[Resources]
        Resources are loaded together with user requests and are thus irrelevant for the analysis. All requests that have the value\\ \stringvalue{org.apache.wicket.request.handler.resource.ResourceReferenceRequestHandler} for the \field{eventTargetClass} field are requests for one of the following file types: jpg, png, gif, svg, js, css, pdf, csv, cls, doc, docx, txt, xml, ttf or woff. Additionally, requests with \field{requestedUrl}s that contain the value \\\stringvalue{nl.topicus.iridium.web.resource.ImageResourceReference} were removed.
        Image requests were also discarded by matching on values \stringvalue{pagetitle:image}, \stringvalue{pagetitle:iconContainer:image} or \stringvalue{title:iconContainer:image} values in the \field{componentPath} field.
    \item[Screensaver]
        As mentioned before in \cref{subsubsec:featurediscovery:results:features:misc}, \somtoday\ has a security measure implemented to protect against malicious students. After a short period of inactivity, a screensaver pops up, forcing a teacher to input his password. A request is made each time the screensaver is enabled or dismissed. These have the value \stringvalue{screensaver} in the \field{componentPath} field.
    \item[Timed updates]
        Some requests are caused by a client-side timer that means to update a component on the page, e.g. the calendar requesting any changes to the current view. These log entries have the value\\ \stringvalue{org.apache.wicket.ajax.AjaxSelfUpdatingTimerBehavior} for the \field{behaviorClass} field.
    \item[Semantically useless pages]
        \napi[C]\ log entries that have a \texttt{NULL} value for their \field{response\_pageClass}, \field{event\_pageClass} and \field{componentPath} fields are useless for this project. Two examples of practical occurrences are with resource requests and when redirecting. In the latter, Wicket adds two log entries: one in which solely the URL contains all information and one in which the rest of the fields hold all the information. The former is discarded by this filter.
    \item[Lazily loaded content]
        These requests always follow regular page request within mere milliseconds. They have the values\\ \stringvalue{org.apache.wicket.extensions.ajax.markup.html.AjaxLazyLoadPanel\$1} or\\ \stringvalue{nl.topicus.iridium.web.components.panel.AjaxLazyLoadPanel\$1} for the \field{behaviorClass} field and values \stringvalue{lazyContentPanel}, \stringvalue{dossiers} or \stringvalue{dataview} for the \field{componentPath} field.
\end{description}

The final and most unexpected issue encountered seems to be a bug in Drill: some queries returned results non-deterministically. There was a pattern to it, but instead of diving into the inner workings of Drill, the whole data preprocessing process was broken down into its most basic parts. Topicus' data storage workflow stores each batch of data in a separate directory. Drill's flexibility allows for easy aggregation of those directories, e.g. by using \texttt{'2016*'} to select every directory starting with \texttt{'2016'}. The solution was to \textit{not} use that flexibility, query each separate directory in each step and only do one specific thing per step. Each step uses the dataset of the previous step. This temporarily consumes 482.6 Gigabytes of storage space, but ensures a validated and working process. The resulting process is:
\begin{enumerate}[align=parleft,labelwidth=5em,labelsep=0.1cm]
	\item[Step CA] This step copies all original log entries to a new directory, casting the relevant fields and discarding the irrelevant ones. Note that this is only applied for the relevant months, as it would be useless to make a casted copy of the complete historical dataset.
	\item[Step UN] This step keeps the log entries that are relevant to the NPS dataset, based on username and organisation.
	\item[Step NP] This step adds the NPS ID to each log entry.
	\item[Step TF] This step applies the time frame filter, discarding log entries that are not within the 90 day period prior the relevant NPS response.
	\item[Step FT] This step applies the action filter as described above. This step does not apply to \api\ log entries, seeing as they do not need action filters.
	\item[Step DU] This step removes duplicate entries using a SELECT DISTINCT statement.
	\item[Step FN] This step simply copies all entries from the working directory to a final directory.
\end{enumerate}
Each step is validated as described above. This process is applied to both \napi\ and \api\ logs. The total amount of queries, taking into account the directories, steps and log types, is 5025. These took about 3.5 to 4 hours to complete and another 10 minutes to validate. \\


% Optimization.
The time necessary for Drill to execute the preprocessing queries was optimized by discarding several columns. Which ones would be discarded was determined by looking at their usefulness for later analysis. For example, all fields relating to the user's device and browser were discarded due to their irrelevance in this study. The impact of this optimization was substantial because of the column-based nature of Drill's Parquet file storage format. In one instance during query construction, omitting one column resulted in an execution duration dropping from about 45 minutes to about 15 minutes.

% Basic analysis.
\Cref{fig:dataprep:logs:respdist} shows the histogram of the relevant log entries per NPS response. The average is 1408.2 log entries with a standard deviation of 3296.9. \Cref{fig:dataprep:logs:tempdist} shows the histogram of the relevant log entries per date. The weekends and holidays are clearly visible. Additionally, the tails at both boundaries are expected due to the overlap of relevant NPS response log periods.

\begin{figure}[h!]
	\centering
	\begin{adjustbox}{center}
		\includesvg[width=1.25\textwidth]{Figures/ppdata-logs-respdist}
	\end{adjustbox}
	\caption{Response distribution of relevant log entries per NPS response}
	\label{fig:dataprep:logs:respdist}
\end{figure}
\begin{figure}[h!]
	\centering
	\begin{adjustbox}{center}
		\includesvg[width=1.25\textwidth]{Figures/ppdata-logs-tempdist}
	\end{adjustbox}
	\caption{Temporal distribution of relevant log entries}
	\label{fig:dataprep:logs:tempdist}
\end{figure}

% Common web usage mining issues.
A few of the common issues in the field of web usage mining are user identification, caching, and grouping requests into distinct sessions~\cite{Srivastava2000,Cooley1997,Cooley1999}. User identification is no issue in \somtoday\ and \somdocent, seeing as user authentication is required to use the system. Wicket has built-in functionality to exert control over client-side caching. It allows this only for resources such as images. Seeing as we ignore such resources, caching is no issue. Wicket also handles sessions. However, seeing as it does this in the context of security, it does not conform to what is required for our purposes. A quick analysis of sessions specified by Wicket shows that some sessions lasted for hours, which does not mean the user actually was active for hours. For example, the session length is increased significantly due to use of the screensaver. To mitigate this, a custom session grouping was implemented. 
An expiration time commonly used in commercial applications is 30 minutes~\cite{Cooley1999}. A time of 60 minutes was chosen for this project so that system activity at the start and end of a class is registered into the same session.
Assigning session IDs is done in three stages. First, for log entries of both log types, the NPS ID and timestamp is extracted. Next, each log entry is assigned information about the timestamp of the previous and next timestamps having the same NPS ID, but only if the time difference between the current and previous or next timestamps is smaller than the session expiration time. In the final stage, each log entry without a previous timestamp is considered the start of a new session. A session starting request and all next requests (grouped per NPS ID) up until but excluding the next session starting request, receive the same session ID. The session ID consists of the NPS ID and an integer that equals the amount of preceding requests without a previous timestamp. The result is a table with the columns described in \cref{tab:dataprep:sessions} and that contains only the bare essentials necessary to identify user activity.

\begin{table}[h!]
    \centering\bgroup\def\arraystretch{1.3}
    \caption{Format for the sessions dataset}
    \label{tab:dataprep:sessions}
    \begin{tabular}{p{2cm}lp{8cm}}
    	\hline
    	Field              & Data type               & Description                                                                                       \\ \hline\hline
    	NPS ID             & string                  & A reference to the associated NPS response.                                                       \\
    	timestamp          & timestamp               & The timestamp of the request.                                                                     \\
    	previous timestamp & timestamp|\texttt{NULL} & The timestamp of the previous request, but only if $(current - previous) < session\, expiration$. \\
    	next timestamp     & timestamp|\texttt{NULL} & The timestamp of the next request, but only if $(next - current) < session\, expiration$.         \\
    	session ID         & string                  & The generated session ID.                                                                         \\ \hline
    \end{tabular}\egroup
\end{table}



%----------------------------------------------------------------------------------------
%	SECTION 3
%----------------------------------------------------------------------------------------
\section{Feature-specific: system usage}
\label{sec:dataprep:usage}
To be able to extract data features \creffulldf{df:prev:total}, \creffulldf{df:prev:total:napi}, \creffulldf{df:prev:total:api} and \creffulldf{df:prev:total:switch}, first the data must be put into the format shown in \cref{tab:dataprep:usage:goal}.
\begin{table}[h!]
    \centering\bgroup\def\arraystretch{1.3}
    \caption{Target format for the system usage preprocessed dataset.}
    \label{tab:dataprep:usage:goal}
    \begin{tabular}{p{2.3cm}p{2.3cm}p{8.4cm}}
    	\hline
    	Field                 & Data type   & Description                                                                                 \\ \hline\hline
    	\field{NPS ID}        & string      & A reference to the associated NPS response.                                                 \\
    	\field{timestamp}     & timestamp   & The timestamp of the request, accurate to the millisecond.                                  \\
    	\field{logtype}       & enumeration & A value indicating the log type.                                                            \\
    	\field{nextTimestamp} & timestamp   & The timestamp of the next request corresponding to the same NPS response.                   \\
    	\field{nextLogtype}   & enumeration & A value indicating the log type of the next request corresponding to the same NPS response. \\ \hline
    \end{tabular}\egroup
\end{table}

The first step was to combine the requests of the \napi\ and \api\ datasets into one. Each entry received a value indicating its logtype. Only the \field{NPS ID}, \field{timestamp} and \field{logtype} fields were necessary. The next step made sure that each log entry received the timestamp and logtype of the subsequent log entry of the same NPS ID. The pattern extraction step aggregates this dataset and is discussed in \cref{sec:patternextraction:usage}.

The reason this dataset only includes timestamps and logtypes is that these data features exclusively concern system use itself and nothing more.


%----------------------------------------------------------------------------------------
%	SECTION 4
%----------------------------------------------------------------------------------------
\section{Feature-specific: repetitive tasks}
\label{sec:dataprep:repetitivetasks}
To extract patterns of repetitive tasks for \creffulldf{df:rep:pat}, \creffulldf{df:rep:iter} and \creffulldf{df:rep:weighted}, the data has to be processed into the desired dataset format listed in \cref{tab:dataprep:rep:goal}. The dataset contains both logtypes.

\begin{table}[h!]
    \centering\bgroup\def\arraystretch{1.3}
    \caption{Target format for the repetitive tasks preprocessed dataset}
    \label{tab:dataprep:rep:goal}
    \begin{tabular}{p{2.3cm}p{2.3cm}p{8.4cm}}
    	\hline
    	Field              & Data type & Description                                                \\ \hline\hline
    	\field{NPS ID}     & string    & A reference to the associated NPS response.                \\
    	\field{timestamp}  & timestamp & The timestamp of the request, accurate to the millisecond. \\
    	\field{session ID} & string    & The generated session ID.                                  \\
    	\field{action}     & string    & A string representing the intended action of the request.  \\ \hline
    \end{tabular}\egroup
\end{table}

The session ID is introduced by including the sessions dataset described in \cref{sec:dataprep:logs}. The action string is a concatenation of a few fields. These are described in \cref{tab:dataprep:actionstring:napi,tab:dataprep:actionstring:api}, respectively for the \napi\ and \api\ logtypes. Note that \cref{tab:dataprep:actionstring:napi} does not include fields containing the target page. This is because a click on the same (type of) button is considered the same action and multiple clicks on similar buttons (e.g. going to the profile of student X and going to the profile of student Y) are the same task. Equivalently, the \api\ action string does not include parameter values.
\begin{table}[h!]
    \centering\bgroup\def\arraystretch{1.3}
    \caption{Concatenated fields for the action string of the \napi\ logtype.}
    \label{tab:dataprep:actionstring:napi}
    \begin{tabular}{p{2.8cm}p{6.7cm}p{3.5cm}}
    	\hline
    	Field                    & Description                                             & Transformations                                                             \\ \hline\hline
    	\field{event\_pageClass} & The Java (page) class that triggered the request.       & Lowercased.                                                                 \\
    	\field{componentClass}   & The component that triggered the request, e.g. a button & Lowercased.                                                                 \\
    	\field{componentPath}    & The path to the component that triggered the request.   & Lowercased and removed any IDs using regular expression '\texttt{:[0-9]+}'. \\ \hline
    \end{tabular}\egroup
\end{table}

\begin{table}[h!]
    \centering\bgroup\def\arraystretch{1.3}
    \caption{Concatenated fields for the action string of the \api\ logtype.}
    \label{tab:dataprep:actionstring:api}
    \begin{tabular}{p{2.8cm}p{7.2cm}p{3cm}}
    	\hline
    	Field                & Description                                                         & Transformations \\ \hline\hline
    	\field{restResource} & The REST resource, with no parameters set.                          & Lowercased.     \\
    	\field{method}       & The method with which the request was sent, e.g. GET, POST, UPDATE. & Lowercased.     \\ \hline
    \end{tabular}\egroup
\end{table}

The resulting dataset (with the format described in \cref{tab:dataprep:rep:goal}) is now a chronologically ordered list of tasks that can be grouped on session and corresponding NPS response. Analysis of repetitive tasks is the next step. 



%----------------------------------------------------------------------------------------
%	SECTION 4
%----------------------------------------------------------------------------------------
\section{Feature-specific: low-hanging fruit}
\label{sec:dataprep:lhf}

Data features \creffulldf{df:school}, \creffulldf{df:edulevels} and \creffulldf{df:npsage} consist mainly of profile data. Topicus supplied XLSX files with mappings from UUID to corresponding schools and UUID to education levels taught, for \cref{df:school,df:edulevels}, respectively. These were preprocessed by Pentaho Kettle/Spoon, again only for simple ETL operations such as string trimming, field renaming and removal and saving into CSV format. \Cref{df:npsage} did not require any preprocessing as its data can be extracted directly from the NPS dataset.
