\chapter{Background}
\label{chp:background}

This section gives background information on all relevant subjects discussed in this thesis, such that no external sources are needed to understand its content. This study focuses on predicting Net-Promoter Scores, explained in \cref{sec:background:nps}, using most of the approach of the field of web mining, outlined in \cref{sec:background:webusagemining}. Machine learning is used to apply the predictions, discussed in \cref{sec:background:machinelearning}. One field of research specific for repetitive patterns is discussed in \cref{sec:background:seq}.


%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------


\section{Net-Promoter Score (NPS)}
\label{sec:background:nps}

% explanation
The Net-Promoter Score is a metric to measure customer loyalty and to some extent customer satisfaction~\cite{Reichheld2003a}. Respondents are asked to answer one question: \textit{"How likely is it that you would recommend our system to a friend or colleague?"}. The answer score is based on a scale of 0 to 10. Respondents giving a 9 or 10 are called \textit{Promoters} and are considered loyal users that stimulate company growth by way of word-of-mouth advertising: they are likely to promote the system to others. Respondents giving a score of 0 to 6 are called \textit{Detractors} and are considered users that are to some extent dissatisfied. Respondents giving a 7 or 8 are called \textit{Passives}. They fall between the categories of Promoters and Detractors and could be labeled as moderately satisfied users who would easily switch to a cheaper system. The system's NPS value is calculated by using $NPS = \frac{Promoters - Detractors}{respondents} \times 100$. Or, put differently:
$$ NPS = (\%Promoters - \%Detractors) \times 100 $$
As such, the range is -100 to 100. Passives are added solely to the total respondents and shift the NPS value closer to zero.

The NPS questionnaire is supplemented with one or more requests for elaboration to the answer on the main question. From a corporate perspective, this is useful for a deeper analysis of user attitude towards the system.

% validity
The validity and reliability of this metric is much debated in scientific studies~\cite{Grisaffe2007,Keiningham2007,Krol2015,Mandal2014,Kristensen2014,Eskildsen2011}. Most of these studies focus on the inability to measure satisfaction and its inherent nuances: only a few such as \cite{Kristensen2014,Eskildsen2011} claim the NPS does not actually measure loyalty. Irrespective of the extent of its validity, companies use the NPS metric to ascertain their overall performance which encourages more research into the subject.




%----------------------------------------------------------------------------------------
%	SECTION 2
%----------------------------------------------------------------------------------------


\section{Web usage mining}
\label{sec:background:webusagemining}

% Web usage mining
The internet is an important part of our lives these days. The Dutch government even went as far as saying that fast internet access is a primary need~\cite{2016nos}. To offer good services to their users, website owners constantly try to improve their websites. This is often done using explicit feedback from their users, but a less time and resource intensive way to collect this data is through the use of web mining~\cite{Cooley1997}. This area focuses on analyzing the available data of a website to discover patterns that can lead to an optimization of the offered services. Web mining can be divided into three categories. \textit{Web content mining} focuses on the information that is served on web pages, \textit{web structure mining} focuses on the links between web pages, and \textit{web usage mining} is all about the behavior of users. 

Analyzing user behavior within an application can give interesting insights. A few of areas using this analysis are decision support in business, marketing and web design, personalization and recommendations \cite{Mobasher2000,Cho2002}, and web caching. Many studies into web usage mining focus on the areas of e-commerce and search engines. Three main tasks are identified with web usage mining~\cite{Srivastava2000}.
\begin{description}
	%~\cite{Cooley1999} can be mentioned concerning several preprocessing tasks.
	\item[Preprocessing] Often referred to as the most time consuming task in the process~\cite{Hussain2010}, preprocessing is about preparing the data for the next task. Difficulties lie (among other things) in identifying individual users, dividing a user's data in different sessions and handling missing data due to cached page views. Sometimes content preprocessing is also necessary. For example, pages can be classified according to their subject or in the case of e-commerce, product class. The same goes for structure preprocessing, e.g. classifying a web site into a hierarchical product class model in e-commerce.
	\item[Pattern discovery] There are several techniques used in web usage mining for pattern discovery. \textit{Statistical analysis} is the most common technique. It looks at descriptive statistics such as frequency, mean and median values about page visits, visit time, visit length, active users, user session length, etc. Another technique is \textit{association rule discovery}. It looks at sets of pages that are often accessed together in a single session, irrespective of whether the pages are directly connected through hyperlinks. For example, in a web shop this might show that users shopping for electronics often also shop for sports clothing. This might indicate to the web shop owner that those categories could be placed closer together. The apriori algorithm is one of the most commonly used algorithms. \textit{Clustering} tries to group pages or users together that have similar properties. This is especially useful when giving personalized recommendations to users. \textit{Classification} attempts to categorize users, pages or sessions into predefined classes. Examples of classifiers are neural networks, decision trees, naive Bayesian and k-nearest neighbors. This can be used for targeted marketing. Finally, \textit{sequential pattern mining} looks at temporal patterns in the scope of multiple sessions. A few examples are trend analysis, change point detection and similarity analysis. This task is called the pattern extraction phase in the current project.
	\item[Pattern analysis] This task intends to draw conclusions from the discovered patterns. Commonly used mechanisms for analysis are SQL and OLAP operations with data cubes. This study uses machine learning to find predictive value in the patterns found in the previous step. The model training phase is aimed at this.
\end{description}



%----------------------------------------------------------------------------------------
%	SECTION 3
%----------------------------------------------------------------------------------------

\section{Machine learning}
\label{sec:background:machinelearning}

% definition
Naqa et al. describe machine learning as "an evolving branch of computational algorithms that are designed to emulate human intelligence by learning from the surrounding environment"~\cite{ElNaqa2015}. Arthur Samuel coined machine learning as a term in 1959, defining it as "the field of study that gives computers the ability to learn without being explicitly programmed"~\cite{Samuel1959}. Put differently, machine learning algorithms learn a mapping function from data, such that they can generalize that to new, unseen data. \\

% process
The general process of building a machine learning model is as follows. One starts with a dataset in which each row is a data point called a \textit{sample} and each column is a \textit{feature} (sometimes also called an \textit{attribute}). These features are values for each sample that are used by the model to make its predictions. This dataset is split into two subsets: a training and a test dataset. This training dataset is used to train the model, after which the model's performance is measured by applying the model on the test dataset. This performance can be increased by preprocessing the dataset, e.g. using normalization or outlier removal. A well-performing model is highly generalizable to new data.

% (un)supervised learning
The two largest directions of machine learning are supervised~\cite{Song2007} and unsupervised learning~\cite{Dy2004,Mitra2002}. \textit{Supervised} learning makes a predictive generalization based on a training dataset where the \textit{target attribute} (or \textit{label}) is known. If the outcome data type is discrete, it uses classification methods. If the outcome data type is continuous, it uses regression methods. \textit{Unsupervised} learning is not based on training data. It tries to discover patterns based only on input data. There are also other directions. Examples are semi-supervised learning~\cite{Chapelle2009}, in which only part of the classes of the training data is known, and reinforcement learning~\cite{Sutton1998}, in which a model learns based on continuous feedback. This study uses supervised learning since the focus is on training a model so that it can make predictions. \\

% preprocessing methods: normalization, outlier removal, class upsampling, handling missing values, discretizing
The dataset used for training has to be of certain quality. If the dataset is full of errors, missing data or otherwise of low quality, the model becomes useless. Several preprocessing methods can be used to deal with this.

Normalization can be applied to prevent dominance of certain features and prevent computing problems. This technique puts the features in the same range, for example by subtracting each value from that feature's mean and dividing by the feature's standard deviation.

A dataset might contain samples that are exceptions to the general rule and are thus not representable. There are various algorithms to detect such outliers, such as DIS, kNN and RNN~\cite{Zhang2009}.
% Aggarwal (2001) has more citations to examples.

Most models can not deal with missing values in the dataset. One solution is to simply discard a sample that does not have values for each feature. This has several drawbacks such as adding bias and decreasing representativeness. A better solution is data imputing. This is a concept similar to interpolation: it replaces missing data with estimated values.

Another factor that can impact a dataset's representativeness is class imbalance. If 90\% of samples are of class A, a model can obtain 90\% accuracy by always predicting class A. The effects of class imbalance can be mitigated in several ways. Collecting more data is an important one, although rarely feasible. Penalized models put more weight on misclassifying the underrepresented classes during training. Upsampling (or oversampling) copies samples from the underrepresented class, while downsampling (or undersampling) removes samples from the overrepresented class. Both resampling methods even out the amount of samples in the classes. Upsampling can also be applied by generating synthetic samples. The most commonly used algorithm is SMOTE~\cite{Chawla2002}, which uses an element of randomness to mitigate the chance of overfitting. \\

% variance, bias, overfitting
One pitfall that must be avoided with predictive modeling is being on one end of the bias-variance tradeoff. High variance means that a model is sensitive to small fluctuations in the training set. The model then fits the training data too well, but not any unseen data: the model is not generalizable. This is also known as overfitting. On the other hand, bias means too many assumptions are made and the model is thus not sensitive enough to the training set, resulting in underfitting.

\begin{comment}
	controlling model complexity:
		Regularization
		ridge regression
		lasso
		subset selection (forward and backward)
		residual sum of squares
	dimensionality reduction:
		principal component analysis
\end{comment}

% performance measurements: cross-validation and metrics
One commonly used method of evaluating a model's performance is k-fold cross-validation~\cite{Stone1974}. The dataset is split into $k$ equally sized subsets. For $k$ times, $k-1$ subsets are used as the training dataset while the other one is used as the test dataset. The performances of the $k$ evaluations are averaged. $k$ is often chosen to be 10. This method eliminates the chance that a model's performance evaluation is an outlier.

Model performance can be measured with different metrics. The error in this context is the difference between a predicted value and the value of the target attribute of a sample. For regression models, the most commonly used metrics are $R^2$, mean squared error (MSE), root mean squared error (RMSE) and mean absolute error (MAE). $R^2$, also known as the coefficient of determination, is a measure of the variance explained by the model ranging from 0 to 1. If this value is 1 it means all variance in the data is explained by the model, but this most often means the model is a victim of overfitting. The MSE takes the mean of all squared errors, as the name suggests. The RMSE does the same and takes the root, changing the unit of this error metric back to the target attribute's unit. The MAE again does what the name suggests and has the added advantage that the metric is better comprehensible. Both the RMSE and MAE have the advantage that they are in the original unit of the predicted value. The RMSE is more influenced by large error outliers, whereas the MAE is more affected by the error variance.

For classification, commonly used metrics are the confusion matrix and a few of its derivatives: precision, recall and accuracy. Each row specifies the predicted classification, each column shows the true classification and each cell contains the amount of samples that have that combination of predicted and true value. The precision of each classification measures the proportion of accurate predictions to the predictions made for that classification. The recall of each classification measures the proportion of accurate predictions to the actual samples of that classification. The accuracy is the proportion of accurate predictions to the total amount of samples. \\

% models
A few model concepts are outlined here. Note that each model has numerous variations, each with their own advantages and disadvantages. Several approaches can also be combined, either by joining the outputs~\cite{Wolpert1992,Breiman1996} or by integrating multiple algorithms~\cite{Biou1988}. Each one listed below is used in this study, be it the original algorithm or a variation based on the original.
\begin{description}
%	%           logistic regression
%	\item[Logistic regression]
%	This is an algorithm that estimates the probability of a certain outcome based on the available features. A large advantage of this algorithm is that, because it works with probabilities, one can immediately see the effect of a specific feature as a change in the overall probability of a certain outcome. This model can be made very flexible by including so-called interaction terms, although too much flexibility may lead to overfitting~\cite{Dreiseitl2002}.
	%           decision trees
	\item[Decision trees]
	This represents a tree in which a decision is made at each node based on a feature (e.g. "is $x_1 \ge 2$?"). This way, the tree is traversed and ultimately a leaf node is selected, which represents a classification. Common algorithms are ID3, C4.5 and CART. A few advantages of a decision tree model are its simplicity, capability to handle both continuous and discrete data, capacity to process large datasets, and above all: its understandability to humans. Its simplicity also gives rise to its disadvantages: decision trees tend to be relatively inaccurate and are prone to failures if the dataset changes too much. Variations on this model used in this project are: decision stumps, which are decision trees of depth 1; random trees, which are decision trees that use random subsets of features; random forests, which are ensembles of decision trees that mitigate the chance of overfitting; and Chi-square automatic interaction detection (CHAID), which prunes decision trees based on the chi-squared attribute relevance test.
	%           neural networks
	\item[Artificial neural networks]
	These simulate the behavior of the human brain. It consists of many, simple units (neurons) that are activated by other neurons or input sensors connected to the environment. The connections between neurons are weighted. Learning occurs by changing the weights of connections until desired behavior results from the specific configuration. An advantage of neural nets is that they perform better at incremental learning. Their accuracy is similar to decision trees, although training time is much longer~\cite{Eklund2002,Lim2000}. One of the largest disadvantages is the lack of explanation. The logical steps of the process with which a neural network algorithm comes to a result is practically impossible to comprehend by humans. Neural nets can be used for both classification and regression tasks. Deep learning is a variation that uses multiple hidden layers of neurons.
	%			Naive Bayes
	\item[Naive Bayes]
	These classifiers are based on Bayes' theorem. They assume all features are independent, simplifying the probabilistic calculations. This assumption is rarely true, hence the 'naive' term. Despite the independence assumption, Naive Bayes classifiers often perform competitively. They are trained fast and scale well.
	%           Support Vector Machines
	\item[Support Vector Machines]
	A Support Vector Machine places one or more separating hyperplanes between the data points in the training set. It tries to maximize the distance to all data points of the different classes. The side on which the data points are with respect to the hyperplane determines which class they belong to. An important advantage of this classifier is its independence on the amount of features. However, this technique has a fairly long training time and it needs the data to be in similar ranges. It can also work with regression problems.
	%			Rule induction
	\item[Rule induction] 
	These iteratively add and remove rules to come up with an optimal set. Rule induction models are similar to decision trees, as they both use rules. An advantage is that rules are easily understandable by humans and computers (i.e. they can be expressed in programming language syntax without effort) and one's own rules can be manually added. A disadvantage is that the model does not scale.
	%			Linear regression
	\item[Linear regression]
	% how does it work?					
	Regression works with numerical values and tries to predict a continuous value. Linear regression tries to fit a linear equation to the observed data. Polynomial regression tries to do the same, but with a higher order equation. The Generalized Linear Model (GLM) is an optimized variation on linear regression that is more flexible. Linear regression works well when the relationship between the features and the target attribute is linear (or polynomial), but can not properly handle situations where this is not the case.
	%			k-Nearest Neighbors
	\item[k-Nearest Neighbors]
	This model looks at a sample's $k$ nearest neighbors. The distance to its neighbors is calculated in $n$-dimensional space based on its $n$ features. An example distance metric is the Euclidian distance (e.g. in 1-dimensional space, the distance between point (3) and point (5) is 2). In the case of classification, it classifies the sample based on the majority of the neighbors. With regression, the predicted value is the neighbors' average. It can be useful to let closer neighbors weigh more than neighbors farther away. Advantages are that this method is robust with non-linear relationships (as mentioned in the description for linear regression) and it is relatively simple. However, its training time is long and is sensitive to unbalanced datasets.
\end{description}


%----------------------------------------------------------------------------------------
%	SECTION 4
%----------------------------------------------------------------------------------------

\section{Sequential pattern mining}
\label{sec:background:seq}
% https://www.coursera.org/learn/data-patterns/lecture/ZJHTA/5-4-prefixspan-sequential-pattern-mining-by-pattern-growth
% http://virtual.vtt.fi/virtual/datamining/publications/miningsequentialpatterns.pdf
% https://www.slideshare.net/Krish_ver2/53-mining-sequential-patterns

Agrawal and Srikant introduced the problem of sequential pattern mining in 1995~\cite{Agrawal1995}. Their goal was to find patterns in sequences, specifically with the use case of customer transactions in retail. In the scenario of video rentals: if customers typically rent "Star Wars", then "Empire Strikes Back" and then "Return of the Jedi", a sequential pattern is identified. The input is a database of customer transactions, having three fields: customer identification, transaction order and the items contained in that transaction. The transaction order field is often a date and/or time value. Continuing the video rental example, \cref{tab:background:customertransactions} shows a possible customer transaction database. Quantities are not taken into account.
\begin{table}[h!]\centering\bgroup\def\arraystretch{1.3}
	\caption{Example customer (video rental) transaction database used as input for sequence pattern mining}
	\label{tab:background:customertransactions}
	\resizebox{1.0\textwidth}{!}{
	\begin{tabular}{llll}
		\hline
		customer ID & transaction date & items                                 & \nl{items\\(shortened)} \\ \hline\hline
		1           & 2011-03-01       & (Battlestar Galactica, Star Wars)     & \textit{(B, S)}         \\
		1           & 2011-03-03       & (Empire Strikes Back)                 & \textit{(E)}            \\
		2           & 2011-05-05       & (Caprica, Star Wars)                  & \textit{(C, S)}         \\
		2           & 2011-05-06       & (Alien, Empire Strikes Back)          & \textit{(A, E)}         \\
		1           & 2011-05-07       & (Minority Report, Return of the Jedi) & \textit{(M, R)}         \\
		2           & 2013-06-07       & (Killjoys, Return of the Jedi)        & \textit{(K, R)}         \\ \hline
	\end{tabular}}\egroup
\end{table}

Each product (or video in this example) is defined as an \textit{item} and each transaction as an \textbf{unordered} \textit{itemset}, \textit{event} or \textit{element}. Note that the parentheses are usually omitted when denoting an itemset of length one. Items in the same itemset are written in alphabetic order, even though they are in an unordered set. To transform a customer transaction database into a format used for sequential pattern mining, each itemset is grouped per customer and ordered by transaction date. An ordered list of transactions is called a \textit{sequence}. The keys of a sequence database are only used as sequence identifiers. \Cref{tab:background:terminologymapping} shows the mapping of the terminology used in literature onto the real-life example. The purpose of \cref{tab:background:terminologymapping} is to aid in understanding the original problem of Agrawal and Srikant.

\Cref{tab:background:seqdb} shows the sequence database of our example. This is the format that is analyzed in sequential pattern mining algorithms. SID is short for sequence ID.


\begin{table}[h]\centering\bgroup\def\arraystretch{1.3}
	\caption{Sequential pattern mining terminology mapping}
	\label{tab:background:terminologymapping}
	\begin{tabular}{llp{3.4cm}}
		\hline
		terminology of \cite{Agrawal1995} & terminology in a real-life example & example value                                                    \\ \hline\hline
		item                              & product (or video)                 & \textit{S}                                                       \\
		itemset/element                   & transaction                        & \textit{(BS)}                                                    \\
		sequence                          & customer history                   & \seq{(BS)E(MR)}                                             \\
		sequence database                 & transaction history                & \{1: \seq{(BS)E(MR)}, \newline 2:~\seq{(CS)(AE)(KR)}\} \\ \hline
	\end{tabular}\egroup
\end{table}
\begin{table}[h]\centering\bgroup\def\arraystretch{1.3}
	\caption{Example sequence database}
	\label{tab:background:seqdb}
	\begin{tabular}{cl}
		\hline
		SID & sequence           \\ \hline\hline
		 1  & \seq{(SB)E(RM)}    \\
		 2  & \seq{(SC)(EA)(RK)} \\ \hline
	\end{tabular}\egroup
\end{table}

Sequential patterns have to adhere to more requirements. The minimum \textit{support} (or \textit{minsupport}) is defined as the minimum amount of sequences a pattern occurs in. Patterns that occur at least in \textit{minsupport} sequences are called \textit{frequent patterns}. Additionally, frequent patterns must be \textit{maximal}. This dictates that a pattern can not be a subpattern of another frequent pattern.

Applying this to our example and setting minsupport to 2, pattern SER is considered a frequent. Pattern \spattern{SE} is not: it is a subpattern of \spattern{SER}, which means it is not maximal. \\

Countless algorithms have been developed since the initial problem was first proposed by Agrawal and Srikant. Each approach since then has had its optimizations. In addition, numerous variations of the original problem required specifically tailored solutions. Giving an introduction to several of the main approaches will support comprehension of the field of sequential pattern mining. All approaches use the apriori property defined by Agrawal and Srikant that says that a pattern is frequent only if all of its subpatterns are frequent. The AprioriAll algorithm~\cite{Agrawal1995} was the first to use this principle.

% GSP
The Generalized Sequence Pattern (GSP) algorithm~\cite{Srikant1996} was the first practical algorithm that built upon AprioriAll. Each item is counted during the first database pass and all non-frequent items are discarded. A loop is entered in which each iteration finds patterns of increasing length. In the candidate generation step, pairs found in the previous iteration are merged. If, for example, the previous iteration found \spattern{abc} and \spattern{bcd}, these are merged into \spattern{abcd}. In the candidate pruning step, candidates are pruned that contain an infrequent subsequence. \spattern{abcd} would be pruned if \spattern{acd} was infrequent. Candidates are eliminated in the next step if their support proves to be insufficient, based on a traversal of the sequence database. This loop continues until no candidates or no frequent patterns can be found. Disadvantages of this approach are the amount of database passes and the amount of generated candidates. A strength compared to AprioriAll is the candidate pruning. It uses a horizontal format, illustrated in \cref{tab:background:seqdb,tab:background:seqdb:spade}. \\
% A difference with AprioriAll is that it adds the options of minimum and maximum time between itemsets, a sliding time window (temporally restricting subsequent itemsets) and the use of taxonomies.

% SPADE
The algorithm Sequential Pattern Discovery using Equivalent Classes (SPADE)~\cite{Zaki2001} uses a vertical format. Take the sequence database shown in \cref{tab:background:seqdb:spade}, with \textit{minsupport=2}. SPADE first converts this to the vertical format shown in \cref{tab:spade:vert}. EID is short for element ID (or event ID), of which the values are shown in factors of ten to emphasize the distinction between the SIDs. The conversion to vertical format requires only one database scan. Next, the \textit{ID lists} of each item are generated, partially illustrated in \cref{tab:spade:idlist1}. The ID lists of infrequent items are discarded, such as item e in our example. Candidates of length 2 can be found by joining rows of the ID lists of candidates of length 1: if they share the same SID and if their EID is sequential, join them. \Cref{tab:spade:idlist2} shows the result of joining a with b and vice versa, and \cref{tab:spade:idlist3} shows the result of joining ab with ba. This continues until no frequent patterns can be found anymore. SPADE reduces the amount of database scans, but still requires large sets of candidates.

\begin{table}[h]\centering\bgroup\def\arraystretch{1.3}
	\caption{Example sequence database for the SPADE algorithm}
	\label{tab:background:seqdb:spade}
	\begin{tabular}{cl}
		\hline
		SID & sequence              \\ \hline\hline
		 1  & \seq{a(abc)(ac)d(cf)} \\
		 2  & \seq{(ad)c(bc)(ae)}   \\
		 3  & \seq{(ef)(ab)(df)cb}  \\
		 4  & \seq{eg(af)cbc}       \\ \hline
	\end{tabular}\egroup
\end{table}

\begin{table}[h]
	\caption{Example progression of SPADE}
	\begin{subtable}[t]{.3\linewidth}\centering\bgroup\def\arraystretch{1.1}
		\begin{tabular}{|ccc|}
			\hline
			SID & EID &    itemset     \\ \hline\hline
			 1  & 10  &  \spattern{a}  \\
			 1  & 20  & \spattern{abc} \\
			 1  & 30  & \spattern{ac}  \\
			 1  & 40  &  \spattern{d}  \\
			 1  & 50  & \spattern{cf}  \\ \hline
			 2  & 10  & \spattern{ad}  \\
			 2  & 20  &  \spattern{c}  \\
			 2  & 30  & \spattern{bc}  \\
			 2  & 40  & \spattern{ac}  \\ \hline
			 3  & 10  & \spattern{ef}  \\
			 3  & 20  & \spattern{ab}  \\
			 3  & 30  & \spattern{df}  \\
			 3  & 40  &  \spattern{c}  \\
			 3  & 50  &  \spattern{b}  \\ \hline
			 4  & 10  &  \spattern{e}  \\
			 4  & 20  &  \spattern{g}  \\
			 4  & 30  & \spattern{af}  \\
			 4  & 40  &  \spattern{c}  \\
			 4  & 50  &  \spattern{b}  \\
			 4  & 60  &  \spattern{c}  \\ \hline
		\end{tabular}\egroup
		\caption{Sequence database in vertical format}\label{tab:spade:vert}
	\end{subtable}
	\begin{subtable}{0.7\linewidth}\centering
		\begin{subtable}{\linewidth}\centering
			\begin{tabular}{ccc|cc|cc|c}
				\hhline{~~~-----} &  &  & \multicolumn{2}{c|}{\spathead{a}} & \multicolumn{2}{c|}{\spathead{b}} & \dots \\
				                  &  &  & SID &             EID             & SID &             EID             & \dots \\
				\hhline{~~~=====} &  &  &  1  &             10              &  1  &             20              & \dots \\
				                  &  &  &  1  &             20              &  2  &             30              &       \\
				                  &  &  &  1  &             30              &  3  &             20              &       \\
				                  &  &  &  2  &             10              &  3  &             50              &       \\
				                  &  &  &  2  &             40              &  4  &             50              &       \\
				                  &  &  &  3  &             20              &     &                             &       \\
				                  &  &  &  4  &             30              &     &                             &       \\
				\hhline{~~~-----}
			\end{tabular}
			\caption{ID lists of patterns length 1}\label{tab:spade:idlist1}
		\end{subtable}\par%\bigskip
		\begin{subtable}{\linewidth}\centering
			\begin{tabular}{cc|ccc|ccc|c}
				\hhline{~~-------} &  & \multicolumn{3}{c|}{\spathead{ab}} & \multicolumn{3}{c|}{\spathead{ba}} & \dots \\
				                   &  & SID & EID (a) &      EID (b)       & SID & EID (b) &      EID (a)       & \dots \\
				\hhline{~~=======} &  &  1  &   10    &         20         &  1  &   20    &         30         & \dots \\
				                   &  &  2  &   10    &         30         &  2  &   30    &         40         &       \\
				                   &  &  3  &   20    &         50         &     &         &                    &       \\
				                   &  &  4  &   30    &         50         &     &         &                    &       \\
				\hhline{~~-------}
			\end{tabular}
			\caption{ID lists of patterns length 2}\label{tab:spade:idlist2}
		\end{subtable}\par%\bigskip
		\begin{subtable}{\linewidth}\centering
			\begin{tabular}{cc|cccc|c}
				\hhline{~~-----} &  & \multicolumn{4}{c|}{\spathead{aba}} & \dots \\
				                 &  & SID & EID (a) & EID (b) &  EID (a)  & \dots \\
				\hhline{~~=====} &  &  1  &   10    &   20    &    30     & \dots \\
				                 &  &  2  &   10    &   30    &    40     &       \\
				\hhline{~~-----}
			\end{tabular}
			\caption{ID lists of pattern length 3}\label{tab:spade:idlist3}
		\end{subtable}
	\end{subtable}

\end{table}

% PrefixSpan
An approach that greatly reduces the effort of candidate generation uses pattern growth. Prefix-projected sequential pattern mining, or PrefixSpan, uses this method~\cite{Han2001}. It sees subsequences as prefixes and their corresponding suffixes as projected databases. Patterns are grown by examining frequent patterns in each projected database. We take same example example sequence database again, shown in \cref{tab:prefixspan:sdb}. PrefixSpan first finds all length-1 sequential patterns, i.e. \spattern{a}, \spattern{b}, \spattern{c}, \spattern{d}, \spattern{e} and \spattern{f}. Each pattern is considered a prefix, where the suffix is its projected database. Only the first occurrence in each sequence are taken into account. \Cref{tab:prefixspan:a-pdb} shows the projected database of pattern \spattern{a}, where underscores indicate that the prefix can also occur there. These projected databases are used to grow the pattern. In the case of our pattern \spattern{a}, the frequent items are \spattern{a}, \spattern{b}, \spattern{\_b}, \spattern{c}, \spattern{d} and \spattern{f}. The length-2 patterns with prefix \spattern{a} are found to be \spattern{aa}, \spattern{ab}, \spattern{(ab)}, \spattern{ac}, \spattern{ad} and \spattern{af}. Each is considered a prefix and their projected databases are generated. The projected database of prefix \spattern{aa} consists of \seq{(\_bc)(ac)d(cf)} and \seq{(\_e)}. As none of the items occur in both sequences, there are no frequent patterns. \Cref{tab:prefixspan:ac-pdb} shows the projected databases of prefix \spattern{ac}. This contains frequent patterns \spattern{a}, \spattern{b} and \spattern{c}, resulting in the length-3 patterns with prefix \spattern{ac} being \spattern{aca}, \spattern{acb} and \spattern{acc}. Doing this again for prefix \spattern{acb} results in the projected database in \cref{tab:prefixspan:acb-pdb}. This process is recursively repeated for the frequent patterns found in each projected database until no more frequent patterns can be found.
Even though the projected databases keep shrinking, constructing them physically is costly. This can be minimized by using pseudo-projection: storing the starting index of the projected suffix instead of the whole sequence. This works if the database can be held in memory. If it can't, a combination of physical and pseudo projection can be made.

\begin{table}[h!]\centering
	\caption{Example progression of PrefixSpan}
	\begin{subtable}[t]{.31\linewidth}\centering\bgroup\def\arraystretch{1.3}
		\begin{tabular}{cl}
			\hline
			SID & original sequence     \\ \hline\hline
			 1  & \seq{a(abc)(ac)d(cf)} \\
			 2  & \seq{(ad)c(bc)(ae)}   \\
			 3  & \seq{(ef)(ab)(df)cb}  \\
			 4  & \seq{eg(af)cbc}       \\ \hline
		\end{tabular}\egroup
		\caption{}\label{tab:prefixspan:sdb}
	\end{subtable}
	\begin{subtable}[t]{.22\linewidth}\centering\bgroup\def\arraystretch{1.3}
		\begin{tabular}{l}
			\hline
			\spathead{a}-projected db \\ \hline\hline
			\seq{(abc)(ac)d(cf)}      \\
			\seq{(\_d)c(bc)(ae)}      \\
			\seq{(\_b)(df)cb}         \\
			\seq{(\_f)cbc}            \\ \hline
		\end{tabular}\egroup
		\caption{}\label{tab:prefixspan:a-pdb}
	\end{subtable}
	\begin{subtable}[t]{.22\linewidth}\centering\bgroup\def\arraystretch{1.3}
		\begin{tabular}{l}
			\hline
			\spathead{ac}-projected db \\ \hline\hline
			\seq{(ac)d(cf)}            \\
			\seq{(bc)(ae)}             \\
			\seq{b}                    \\
			\seq{bc}                   \\ \hline
		\end{tabular}\egroup
		\caption{}\label{tab:prefixspan:ac-pdb}
	\end{subtable}
	\begin{subtable}[t]{.21\linewidth}\centering\bgroup\def\arraystretch{1.3}
		\begin{tabular}{l}
			\hline
			\spathead{acb}-projected db \\ \hline\hline
			                            \\
			\seq{(\_c)(ae)}             \\
			                            \\
			\seq{c}                     \\ \hline
		\end{tabular}\egroup
		\caption{}\label{tab:prefixspan:acb-pdb}
	\end{subtable}
\end{table}



%TODO: ! mention a bit about cyclic patterns and specifically the minrepsupport requirement. also max_gap?
