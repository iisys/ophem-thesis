from collections import defaultdict

def apriorirep1i(sequence_db, minsupport, minrepsupport, minpatternlen=2):
    '''
        A Python (2.7) implementation of the apriorirep1i algorithm, based on AprioriAll. More information and an online version can be found at https://karim.elass.al/masterthesis/.
        The return value is an occurrence set: a dictionary where the keys are tuples of frequent patterns, the values are dictionaries where, in turn, the keys are the sequence indices and the values are sets of integers. Those integers are the indices indicating where the frequent pattern starts in the sequence.
        The value of minsupport is considered to be absolute and minrepsupport must be larger than 1.
    '''
    
    # Sort phase is already done when receiving sequences.
    
    # Litemset phase.
    Lk = get_l1items(sequence_db, minsupport, minrepsupport)
    l1items = Lk.keys()
    
    # Transformation phase.
    # Truncate irrelevant sequences instead of removing all non-l1items: else the occurrence set data structure doesn't work anymore.
    Dt = truncate_nonlitem_sequences(sequence_db, l1items)
        
    # Sequence phase.
    k = 2
    # Lk is the occurrence data of k-1, L is the occurrence data of k.
    L      = defaultdict(lambda:defaultdict(set)) # occurrence data
    result = defaultdict(lambda:defaultdict(set)) # occurrence data
    while len(L) > 0 or k == 2:
        
        L = get_next_patterns(Lk, Dt)
        L = purge_nonlpatterns(L, minsupport, minrepsupport)
        
        # Purge occurrences of patterns that are a starting cycle of its subpattern (e.g. abca in abcabc with subpattern abc).
        L = purge_subpattern_cycle_start(L, Lk, Dt)
        
        # Purge occurrences of patterns that are a self-succeeding cyclically shifted variant (e.g. bca and cab in abcabcabc).
        L = purge_cyclic_shifts(L, Dt)
        
        # Enforce minsupport and minrepsupport requirements.
        # Necessary because irrelevant patterns shouldn't be taken into account in the next step.
        L = purge_nonlpatterns(L, minsupport, minrepsupport)
    
        # Maximal phase.
        if k-1 >= minpatternlen:
            
            # Purge subpatterns that connect two consecutive occurrences of a superpattern (e.g. ca in abcabc).
            Lk = purge_interconnecting_subpatterns(Lk, L)
            
            # Purge subpatterns that are actually part of a superpattern (e.g. ab in abcabc).
            Lk = purge_subpatterns(Lk, L)
            
            # Save Lk.
            result.update(Lk)
        
        # Continue.
        Lk = L
        k += 1
        
    # Include the latest L in the result.
    result.update(L)
    
    # Enforce support requirements for patterns that had their occurrences decreased in the maximal phase.
    result = purge_nonlpatterns(result, minsupport, minrepsupport)
    
    return result

def get_l1items(sequences, minsupport, minrepsupport):
    ''' Return an occurrence data structure of litems. '''
    items = defaultdict(lambda: defaultdict(set))
    for si,sequence in enumerate(sequences):
        for i,item in enumerate(sequence):
            items[(item,)][si].add(i)
    return purge_nonlpatterns(items, minsupport, minrepsupport)

def truncate_nonlitem_sequences(sequences, litems):
    ''' If there is not any litem in a sequence in L, truncate that sequence. '''
    return [
        []
            if not any((item,) in litems for item in sequence) 
            else 
                sequence
        for sequence in sequences
    ]
    
def get_next_patterns(Lk, Dt):
    ''' Generate patterns of length k+1 based on the occurrences of patterns of length k. '''
    L = defaultdict(lambda: defaultdict(set))
    # For each pattern of length k...
    for p, occurrences in Lk.iteritems():
        # si : sequence id/index.
        # psi: 'pattern in sequence' index (start of the pattern).
        # For each pattern occurrence in each sequence...
        for si, psis in occurrences.iteritems():
            for psi in psis:
                # 'item in sequence' index of the litem subsequent to p.
                isi = psi+len(p)
                # Avoid out of bounds exception.
                if len(Dt[si]) <= isi:
                    continue
                # Add new pattern to the result set.
                L[p + (Dt[si][isi],)][si].add(psi)
    return L
    
def purge_nonlpatterns(L, minsupport, minrepsupport):
    ''' Filter an occurrence data structure by minimum sequence and repetitions. '''
    for c, occurrences in L.iteritems():
        L[c] = defaultdict(set, {
            si:s 
            for si, s in occurrences.iteritems() 
                if len(s) >= minrepsupport
        })
        
    return defaultdict(lambda:defaultdict(set),
        {
            c: occurrences 
            for c, occurrences in L.iteritems() 
                if len(occurrences) >= minsupport
        })
    
def purge_subpattern_cycle_start(L, Lk, Dt):
    ''' Purge the occurrences of candidates in L that are starting their self-succeeding cycle, based on occurrences of length k-1 (passed in Lk). '''
    for c in L.keys():
        if c[0] == c[-1] and Lk.has_key(c[0:-1]):
            for si, s in L[c].iteritems():
                # Remove index if Lk[c[0:-1]] has the cyclical occurrence (starting at i and i+length of c[0:-1]).
                L[c][si] = set([
                    i 
                    for i in L[c][si] 
                        if not (i              in Lk[c[0:-1]][si] and
                                i+len(c[0:-1]) in Lk[c[0:-1]][si])
                ])
    return L
    
def purge_cyclic_shifts(L, Dt):
    ''' Purge the occurrences of self-succeeding cyclically shifted variants of a candidate. '''
    # Use a sliding window to determine the first occurrence of a pattern.
    if len(L) > 0:
        l = len(L.keys()[0]) # pattern length
        for si,s in enumerate(Dt):
            cp  = None # current main pattern
            cpi = None # current pattern index
            for i in range(0, len(s)-l+1):
                p = tuple(s[i:i+l])
                
                # Within cyclical pattern range.
                if cp is not None and i < cpi+l:
                    shift = i - cpi
                    cp_shifted = cp[shift:] + cp[:shift]
                    if p == cp_shifted and i in L[cp_shifted][si]:
                        L[cp_shifted][si].remove(i)
                # Outside of cyclical pattern range or no main pattern set yet, and pattern encountered.
                if (cp is None or i >= cpi+l) and i in L[p][si]:
                    cp = p
                    cpi = i
    return L
    
def purge_interconnecting_subpatterns(Lk, L):
    ''' Purge subcandidates that connect two consecutive occurrences of a candidate. '''
    # For each candidate...
    for c,_ in L.iteritems():
        # For each cyclical variant of the candidate...
        for shift in range(-1, -1*len(c), -1):
            # Get the subcandidate (shifted variant of candidate, minus the last element).
            subc_shifted = c[shift:] + c[:shift-1]
            
            # For each sequence that the subcandidate occurs in...
            for si, s in Lk[subc_shifted].iteritems():
                # Remove index if the subcandidate is part of two consecutive occurrences of the main candidate.
                Lk[subc_shifted][si] = set([
                    i
                    for i in Lk[subc_shifted][si]
                        if not (i-shift-len(c) in L[c][si] and
                                i-shift        in L[c][si])
                ])
    return Lk
    
def purge_subpatterns(Lk, L):
    ''' Purge non-cyclical subcandidates that are part of the larger candidate. This must happen after cyclical subcandidates are processed, not before. '''
    # For each candidate...
    for c,occurrences in L.iteritems():
        j = len(c)-1 # Length of the candidates in Lk
        # For each shift variation of the candidate...
        for shift, subc in [(0, c[0:-1]), (1, c[1:])]:
            
            # For each sequence of the candidate...
            for si, s in L[c].iteritems(): # No list comprehension: we overwrite only the relevant sequence indices.
                # Only keep the occurrences that are not part an occurrence of the main candidate.
                Lk[subc][si] = set([i for i in Lk[subc][si] if i-shift not in L[c][si]])
    return Lk