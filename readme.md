# Master thesis
Hi! This is my master thesis, codenamed Ophem. All relevant files for the end result can be found on <https://karim.elass.al/masterthesis/>.

This is the timeline:

+ May 2017 - end of August 2017: full time work.
+ September 2017 - start of November 2017: no work due to medical reasons.
+ November 2017 - April 2018: part time work.
+ April 24th, 2018: final deadline.
+ May 1st, 2018: defence.

### To compile...
1. Make sure you have [Python](https://www.python.org/downloads/) 2.7 installed.
2. Run `pdflatex` with the `-shell-escape` option. This is to ensure the proper working of the `minted` package, used for making code listings beautiful.

At some point I had some issues with converting files from SVG to PDF. I used the batch script in /Figures/ to use Inkscape for conversion. This only has to be done one time, so it is not necessary for compilation.