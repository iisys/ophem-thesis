@echo off

for %%f in (*.svg) do (
    del %%~nf.pdf
    del %%~nf.pdf_tex
)
echo Deleted all *.pdf and *.pdf_tex files.
echo.
for %%f in (*.svg) do (
    echo Working on %%~nf...
    start inkscape -D -z --file=%%~nf.svg --export-pdf=%%~nf.pdf --export-latex
)
pause